>This README file was generated on 2024-11-02 by contributors to the SchoolDrop'App project.
>
>Last updated on: 2025-01-03.
>
>Project name: SchoolDrop'App - School dropout prediction application (hereinafter referred to as “SchoolDrop'App”).

# SchoolDrop'App

## General information

“SchoolDrop'App” is a predictive analysis application developed in Excel VBA and Python to help identify students at risk of dropping out of school. The user enters explanatory criteria in a VBA form, and this information is then passed on to a Python script which executes the predictions. The predictive model uses machine learning methods to analyze the data entered and provide an assessment of the risk of dropping out. The tool is designed to support teachers and educational advisors in the early detection of at-risk situations and facilitate appropriate interventions.


## Table of contents 

1. [First steps](#firststep)
2. [Project status](#etat)
3. [Usage](#utilisation)  
    - [Key features](#fonctionnalite)  
    - [Workflows](#flux)  
4. [Run tests](#tests)
5. [Known issues and limitations](#probleme)
6. [Tools](#outils)
7. [Source](#source)
8. [Authors](#auteurs)
9. [License](#licence)


## First steps <a name="firststep"></a>

1. Install the required dependencies, two methods are available:  

    - First method, use the command prompt, go to the root of the project (where the `requirements.txt` file is located) and executing the following command:  

    ```bash
    pip install -r requirements.txt 
    ```

    - Second method, you can run the `requierements.bat` file, which will perform the first method for you.

2. Enable macros in the `app.xlsm` file properties to make the interface functional.  
    - [How to do ?](https://learn.microsoft.com/en-us/microsoft-365-apps/security/internet-macros-blocked#guidance-on-allowing-vba-macros-to-run-in-files-you-trust)

3. Run the excel file `app.xlsm` (available at the root of the file)

4. Click on "Start predicting".

5. Fill in the required informations.

6. Click on "Predict".


## Project status <a name="etat"></a>

The project is in a stable state, all the desired functionalities have been implemented and there will be no further updates.


## Usage <a name="utilisation"></a>

“SchoolDrop'App” is an application integrating a VBA form and Python scripts to predict the risk of dropping out of school among young people. Designed for simple, intuitive use, “SchoolDrop'App” collects personal data via Excel VBA forms and provides a predictive assessment of dropout risk based on this data.


### Key features <a name="fonctionnalite"></a>

**Collecting user data using VBA :**

- **Interactive forms :** The user's personal information is collected via an Excel VBA form. This form includes questions on explanatory variables (e.g. age, education level, school history, family support, etc.) selected for their relevance in predicting the risk of dropping out of school.
- **Data validation :** The form includes input checks to ensure that information is entered correctly, reducing input errors and guaranteeing quality data collection.
	
**Statistical prediction with Python :**

- **Predictive model :** The Python script uses a statistical model, trained on representative data, to estimate the probability of dropping out of school as a function of explanatory variables supplied by the user.
- **Automated calculations :** Once the data has been submitted, the Python script is triggered to process the information and calculate the risk of dropping out.
- **Instant feedback :** Results returned directly to the user via Excel interface.

**Results report :**

- **Results presentation :** “SchoolDrop'App” provides simple visual feedback indicating the student's predicted situation with a prediction reliability indicator, enabling the user to easily understand the associated level of risk.


### Workflows <a name="flux"></a>

- The user fills in the VBA form with his personal information and the necessary explanatory data.
- The form sends this data to the Python script for predictive calculation.
- Python performs the calculations and returns an estimate of the risk of dropping out.
- The result is displayed on the interface.


## Run tests <a name="tests"></a>

All unit tests are in the test directory. These tests have been designed to verify the correct operation of all the functionalities available in our interface, and to ensure that every component of the application meets functional and performance requirements.


## Known issues and limitations <a name="probleme"></a>

The application has no known issues

## Tools <a name="outils"></a>

The “SchoolDrop'App” application has been developed using the following **technologies** :

- **Excel Virtual Basic for Applications (VBA) :** to create the user interface and manage data entered via forms.
- **Python :** for data analysis and implementation of prediction algorithms.

**Python libraries used :**
- Scikit-learn : automatic learning model and prediction.
- Pandas : database management, manipulation and analysis.
- Seaborn : for data visualization.
- Statsmodels : for statistical models and tests.
- Wordcloud : word cloud generation for visual representations.

**Development environment :**
- Developed under Visual Basic Editor for VBA and Visual Studio code, and Spyder for Python.

**Version management tools :**
- Git : for tracking changes and collaboration between team members.


## Source <a name = "source"></a>

This project builds on the work entitled “Predict students' dropout and academic success” carried out in 2021 by : 

- Valentim Realinho,
- Jorge Machado, 
- Luís Baptista, 
- Mónica V. Martins.  

REALINHO, Valentim, MACHADO, Jorge, BAPTISTA, Luís, et al. Predict students’ dropout and academic success [en ligne].  Zenodo, December 13, 2021. [Accessed November 4, 2024]. [DOI 10.5281/zenodo.5777340](https://doi.org/10.5281/zenodo.5777340).


## Authors <a name="auteurs"></a>

This project was developed by 5 students from the “Statistique pour l'Evaluation et la Prévision” (SEP) master's program at the University of Reims Champagne-Ardenne (class of 2024-2025). 

The contributors to this project and their roles are identified below : 

- Séverine NOR : Data Analyst/Governance
- Jawhara CHAFI : Data Scientist
- Julien PHAM : Frontend/User Interface
- Mathys GENET : Data Engineer/Scrum Master
- Rémi GOMES MOREIRA : Product Owner


## License <a name="licence"></a>

This project is licensed under the **[Creative Commons Attribution-NonCommercial 4.0 International (CC BY-NC 4.0)](https://creativecommons.org/licenses/by-nc/4.0/)** license. You are free to share and adapt this project for non-commercial purposes, provided you credit the author. No commercial use is permitted without express permission.

License summary :

Attribution : You must credit the original author, provide a link to the license, and indicate whether any modifications have been made. You may do so reasonably, but not in such a way as to suggest that the author endorses you or your use.
Non-commercial : You may not use the material for commercial purposes.

© SchoolDrop'App project contributors - School dropout prediction application.