import pytest
import os
import pandas as pd
import sys
sys.path.append(os.path.abspath(os.path.join(os.path.dirname(__file__), '../../../script/models/random_forest')))
from tools_randomforest import encode,train_random_forest,evaluate_model  # Adaptez le chemin selon votre projet
from sklearn.ensemble import RandomForestClassifier
from sklearn.metrics import accuracy_score

import pytest
import pandas as pd
from sklearn.ensemble import RandomForestClassifier
from sklearn.model_selection import train_test_split
from tools_randomforest import encode, train_random_forest, evaluate_model, graph_important_features
from unittest.mock import patch
import matplotlib.pyplot as plt

# Constantes pour les tests
MAPPING_Y = {"Dropout": 0, "Enrolled": 1, "Graduate": 2}
QUANT_VARS = ['Curricular units 2nd sem (evaluations)','Curricular units 2nd sem (enrolled)','Age at enrollment','Inflation rate']
QUAL_VARS  = ['Tuition fees up to date','Gender','Nacionality','Marital status',"Mother's occupation",'Course']
VAR_PRED = ['Target']
# Données factices
mock_data = pd.read_csv('data/dataset.csv')

# 1. Test de la fonction `encode`
def test_encode():
    df = mock_data
    X_encoded, y = encode(df, intercept=False)

    # Vérifications sur les données encodées
    assert not X_encoded.empty, "Les données encodées ne doivent pas être vides."
    assert len(y) == len(df), "La cible encodée doit avoir la même longueur que les données originales."
    assert "Age at enrollment" in X_encoded.columns, "La colonne Age doit être présente dans les données encodées."

# 2. Test de la fonction `train_random_forest`
def test_train_random_forest():
    X_train = pd.DataFrame({
        "Feature1": [1, 2, 3],
        "Feature2": [4, 5, 6],
    })
    y_train = [0, 1, 1]

    rf_model = train_random_forest(X_train, y_train)

    # Vérification du modèle
    assert isinstance(rf_model, RandomForestClassifier), "Le modèle doit être une instance de RandomForestClassifier."
    assert hasattr(rf_model, "predict"), "Le modèle doit avoir une méthode `predict`."

# 3. Test de la fonction `evaluate_model`
def test_evaluate_model(capsys):
    # Données factices
    X_test = pd.DataFrame({
        "Feature1": [1, 2],
        "Feature2": [4, 5],
    })
    y_test = [0, 1]
    y_pred = [0, 1]

    # Modèle factice
    class DummyModel:
        def predict(self, X):
            return y_pred

    model = DummyModel()

    # Évaluer le modèle
    evaluate_model(X_test, y_test, model)

    # Capturer la sortie
    captured = capsys.readouterr()
    assert "Classification Report" in captured.out, "Le rapport de classification doit être affiché."
