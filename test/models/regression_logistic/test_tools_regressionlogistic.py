import pytest
import pandas as pd
import numpy as np
from sklearn.linear_model import LogisticRegression
from sklearn.metrics import confusion_matrix
import sys 
import os
from constants import VAR_PRED, QUANT_VARS, QUAL_VARS, MAPPING_Y
sys.path.append(os.path.abspath(os.path.join(os.path.dirname(__file__), '../../../script/models/regression_logistic')))
from tools_regressionlogistic import (
    LogRegression, 
    LogRegression_withLassoCV, 
    vif, 
    extract_zero_coefficients, 
    coefRL_without_group, 
    evaluate_model, 
    validate_logistic_regression
)

# Dummy Data for Testing
@pytest.fixture
def dummy_data():
    X_train = pd.DataFrame({
        "feature1": [1, 2, 3, 4, 5],
        "feature2": [2, 4, 6, 8, 10]
    })
    y_train = pd.Series([0, 1, 0, 1, 0])
    X_test = pd.DataFrame({
        "feature1": [1, 2],
        "feature2": [2, 4]
    })
    y_test = pd.Series([0, 1])
    return X_train, y_train, X_test, y_test

def test_LogRegression(dummy_data):
    X_train, y_train, _, _ = dummy_data
    model = LogRegression(X_train, y_train)
    assert isinstance(model, LogisticRegression), "Model is not an instance of LogisticRegression"
    assert model.coef_.shape[1] == X_train.shape[1], "Coefficient dimensions do not match input features"

def test_LogRegression_withLassoCV(dummy_data):
    X_train, y_train, _, _ = dummy_data
    model = LogRegression_withLassoCV(X_train, y_train)
    assert hasattr(model, "C_"), "Model does not have attribute 'C_'"
    assert model.C_ is not None, "Cross-validation did not select a value for C"

def test_vif(dummy_data):
    X_train, _, _, _ = dummy_data
    vif_result = vif(X_train)
    assert isinstance(vif_result, pd.DataFrame), "VIF result is not a DataFrame"
    assert "VIF" in vif_result.columns, "VIF column missing in the result"

def test_extract_zero_coefficients(dummy_data):
    X_train, y_train, _, _ = dummy_data
    model = LogRegression(X_train, y_train)
    zero_coeffs = extract_zero_coefficients(model, X_train.columns)
    assert isinstance(zero_coeffs, list), "Result is not a list"

def test_coefRL_without_group(dummy_data, capsys):
    X_train, y_train, _, _ = dummy_data
    model = LogRegression(X_train, y_train)
    coefRL_without_group(model, X_train.columns)
    captured = capsys.readouterr()
    assert "Coef for" in captured.out, "Output does not contain expected text"

def test_evaluate_model(dummy_data, capsys):
    X_train, y_train, X_test, y_test = dummy_data
    model = LogRegression(X_train, y_train)
    evaluate_model(X_test, y_test, model)
    captured = capsys.readouterr()
    assert "Classification Report" in captured.out, "Output does not contain Classification Report"

def test_validate_logistic_regression(dummy_data, capsys):
    X_train, y_train, _, _ = dummy_data
    model = LogRegression(X_train, y_train)
    validate_logistic_regression(X_train, y_train, model)
    captured = capsys.readouterr()
    assert "p-value of chi-squared" in captured.out, "Output does not contain p-value information"
