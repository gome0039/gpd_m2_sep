DATA = "dataset.csv"

#QUANT_VARS = ['Curricular units 1st sem (evaluations)','Curricular units 1st sem (enrolled)','Curricular units 1st sem (grade)','Age at enrollment']

QUANT_VARS = ['Curricular units 2nd sem (evaluations)','Curricular units 2nd sem (enrolled)','Age at enrollment','Inflation rate']

QUAL_VARS  = ['Tuition fees up to date','Gender','Nacionality','Marital status',"Mother's occupation",'Course']

VAR_PRED = ['Target']

MAPPING_Y = {
    'Graduate': 2,
    'Enrolled': 1,
    'Dropout': 0
    }
df_mapping = {
        'Marital status': {
            1: "Single", 
            2: "Married", 
            3: "Widower", 
            4: "Divorced", 
            5: "Facto Union", 
            6: "Legally Separated"
        },
        "Course": {
            1: "Biofuel Production Technologies", 
            2: "Animation and Multimedia Design", 
            3: "Social Service (evening attendance)", 
            4: "Agronomy", 
            5: "Communication Design", 
            6: "Veterinary Nursing", 
            7: "Informatics Engineering", 
            8: "Equinculture", 
            9: "Management", 
            10: "Social Service", 
            11: "Tourism", 
            12: "Nursing", 
            13: "Oral Hygiene", 
            14: "Advertising and Marketing Management", 
            15: "Journalism and Communication", 
            16: "Basic Education", 
            17: "Management (evening attendance)"
        },
        "Mother's occupation": {
            1: "Student", 
            2: "Representatives of the Legislative Power and Executive Bodies, Directors, Directors and Executive Managers", 
            3: "Specialists in Intellectual and Scientific Activities", 
            4: "Intermediate Level Technicians and Professions", 
            5: "Administrative staff", 
            6: "Personal Services, Security and Safety Workers and Sellers", 
            7: "Farmers and Skilled Workers in Agriculture, Fisheries and Forestry", 
            8: "Skilled Workers in Industry, Construction and Craftsmen", 
            9: "Installation and Machine Operators and Assembly Workers", 
            10: "Unskilled Workers", 
            11: "Armed Forces Professions", 
            12: "Other Situation", 
            13: "(blank)", 
            14: "Health professionals", 
            15: "Teachers", 
            16: "Specialists in information and communication technologies (ICT)", 
            17: "Intermediate level science and engineering technicians and professions", 
            18: "Technicians and professionals of intermediate level of health", 
            19: "Intermediate level technicians from legal, social, sports, cultural and similar services", 
            20: "Office workers, secretaries in general and data processing operators", 
            21: "Data, accounting, statistical, financial services and registry-related operators", 
            22: "Other administrative support staff", 
            23: "Personal service workers", 
            24: "Sellers", 
            25: "Personal care workers and the like", 
            26: "Skilled construction workers and the like, except electricians", 
            27: "Skilled workers in printing, precision instrument manufacturing, jewelers, artisans and the like", 
            28: "Workers in food processing, woodworking, clothing and other industries and crafts", 
            29: "Cleaning workers", 
            30: "Unskilled workers in agriculture, animal production, fisheries and forestry", 
            31: "Unskilled workers in extractive industry, construction, manufacturing and transport", 
            32: "Meal preparation assistants"
        },
        'Gender': {
            0: "Female", 
            1: "Male"
        },
        "Tuition fees up to date": {
            0: "No", 
            1: "Yes"
        }
    }

# QUANT_VARS = ["GDP","Unemployment rate","Curricular units 1st sem (credited)",
#               'Curricular units 1st sem (evaluations)',
#               "Curricular units 1st sem (approved)",'Curricular units 1st sem (enrolled)',
#               "Curricular units 1st sem (grade)","Curricular units 1st sem (without evaluations)",
#               "Curricular units 2nd sem (without evaluations)","Curricular units 2nd sem (grade)",
#               "Curricular units 2nd sem (approved)","Curricular units 2nd sem (credited)",
#               'Curricular units 2nd sem (evaluations)','Curricular units 2nd sem (enrolled)',
#               'Age at enrollment','Inflation rate']

# QUAL_VARS  = ["International","Scholarship holder","Debtor","Educational special needs","Displaced",'Daytime/evening attendance',
#               'Previous qualification',
#               'Nacionality',"Father's qualification","Mother's qualification",
#               "Father's occupation",'Application order','Application mode',
#               'Tuition fees up to date','Marital status',"Mother's occupation",'Gender','Course']