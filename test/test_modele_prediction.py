import os
import pytest
import pandas as pd
import sys

sys.path.append(os.path.abspath(os.path.join(os.path.dirname(__file__), '../script/')))
from modele_prediction import TargetEstimator  # Adaptez le chemin selon votre projet

@pytest.fixture
def estimator():
    """
    Fixture pour créer une instance de la classe TargetEstimator.
    """
    return TargetEstimator()

@pytest.fixture
def sample_data(estimator):
    """
    Fixture pour charger les données via la méthode load_data.
    """
    df = estimator.load_data()
    return df

def test_load_data(estimator):
    """
    Test du chargement des données.
    """
    df = estimator.load_data()
    assert not df.empty, "Le DataFrame chargé est vide."
    assert 'Target' in df.columns, "La colonne cible n'est pas présente dans les données."

def test_encode_variables(estimator, sample_data):
    """
    Test de l'encodage des variables.
    """
    X, y = estimator.encode_variables(sample_data)

    assert 'const' in X.columns, "La constante pour la régression logistique n'a pas été ajoutée."
    assert y.dtype.name == "category", "La variable cible n'est pas encodée comme catégorie."
    assert not X.isnull().any().any(), "Il y a des valeurs manquantes dans les données encodées."


def test_get_prediction(estimator, sample_data):
    """
    Test des prédictions.
    """
    # Préparation des données et formation du modèle
    X, y = estimator.encode_variables(sample_data)
    estimator.get_modele(X, y)

    # Test des prédictions avec des exemples arbitraires
    pred = estimator.get_prediction(2, 5, 25, 3.5, 1, 0, 1, 1, 2)
    assert len(pred) == 3, "Les prédictions ne contiennent pas les trois catégories."
    assert abs(sum(pred) - 1) < 1e-6, "La somme des probabilités prédites n'est pas égale à 1."