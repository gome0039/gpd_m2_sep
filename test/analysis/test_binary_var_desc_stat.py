import os
import pandas as pd
import pytest
import sys
import matplotlib.pyplot as plt
from io import StringIO

sys.path.append(os.path.abspath(os.path.join(os.path.dirname(__file__), '../../script/analysis')))

from binary_var_desc_stat import (
    run_data, select_binaries_var, check_na, check_types, 
    display_dataset, display_stats_desc, display_pie_chart
)


# Mock data for testing
TEST_DATA = """Gender,Feature1,Feature2,Feature3
1,0,1,0
0,1,0,1
1,0,1,1
0,1,0,0
"""

@pytest.fixture
def mock_dataframe():
    """Fixture to create a mock dataframe."""
    return pd.read_csv(StringIO(TEST_DATA))

def test_run_data_valid_file(tmp_path):
    """Test `run_data` with a valid CSV file."""
    file_path = tmp_path / "dataset.csv"
    with open(file_path, "w") as f:
        f.write(TEST_DATA)
    
    df = run_data(str(tmp_path))
    assert isinstance(df, pd.DataFrame)
    assert not df.empty
    assert list(df.columns) == ["Gender", "Feature1", "Feature2", "Feature3"]

def test_run_data_missing_file(tmp_path):
    """Test `run_data` handles missing file gracefully."""
    df = run_data(str(tmp_path))
    assert df is None

def test_select_binaries_var(mock_dataframe):
    """Test `select_binaries_var` to identify binary columns."""
    columns_bin = select_binaries_var(mock_dataframe)
    assert list(columns_bin) == ["Gender","Feature1", "Feature2", "Feature3"]

def test_select_binaries_var_no_binaries():
    """Test `select_binaries_var` when no binary columns exist."""
    data = """Col1,Col2,Col3
    2,3,4
    5,6,7
    """
    df = pd.read_csv(StringIO(data))
    columns_bin = select_binaries_var(df)
    assert len(columns_bin) == 0

def test_check_na(mock_dataframe, capsys):
    """Test `check_na` outputs the correct NA summary."""
    check_na(mock_dataframe)
    captured = capsys.readouterr()
    assert "Feature1" in captured.out
    assert "Feature2" in captured.out
    assert "Feature3" in captured.out

def test_check_types(mock_dataframe, capsys):
    """Test `check_types` outputs the correct data types."""
    check_types(mock_dataframe)
    captured = capsys.readouterr()
    assert "int64" in captured.out

def test_display_dataset(mock_dataframe, capsys):
    """Test `display_dataset` outputs the first 5 rows."""
    display_dataset(mock_dataframe)
    captured = capsys.readouterr()
    assert "Gender" in captured.out
    assert "Feature1" in captured.out

def test_display_stats_desc(mock_dataframe, capsys):
    """Test `display_stats_desc` outputs descriptive statistics."""
    display_stats_desc(mock_dataframe)
    captured = capsys.readouterr()
    assert "Feature1" in captured.out
    assert "count" in captured.out
    assert "mean" in captured.out

def test_display_pie_chart(mock_dataframe, monkeypatch):
    """Test `display_pie_chart` displays a chart correctly."""
    # Mock plt.show to avoid displaying the plot during the test
    def mock_show():
        pass
    monkeypatch.setattr(plt, "show", mock_show)
    
    labels_gender = {0: "Woman", 1: "Man"}
    display_pie_chart(mock_dataframe, "Gender", labels_gender, "Test Chart")
    # Test passes if no exception is raised

