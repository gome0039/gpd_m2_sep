import pytest
import pandas as pd
import numpy as np
import sys
import os
from io import StringIO
import matplotlib.pyplot as plt
sys.path.append(os.path.abspath(os.path.join(os.path.dirname(__file__), '../../script/analysis')))


from multiple_var_desc_stat import (
    run_data, select_multiples_var, calculate_percent, check_na, 
    check_types, display_dataset, display_desc_stat, display_pie_chart,
    display_barplot, display_boxplot, display_wordcloud_
)

# Mock data for testing
TEST_DATA = """Target,Course,Marital status,Mother's occupation,Father's occupation,Var1,Var2
1,7,2,12,15,5.6,3.2
0,10,1,34,20,4.5,2.8
1,3,4,46,10,6.1,3.5
0,7,3,12,15,5.8,3.1
"""

@pytest.fixture
def mock_dataframe():
    """Fixture to create a mock dataframe."""
    return pd.read_csv(StringIO(TEST_DATA))

def test_run_data_valid_file(tmp_path):
    """Test `run_data` with a valid CSV file."""
    file_path = tmp_path / "dataset.csv"
    with open(file_path, "w") as f:
        f.write(TEST_DATA)
    
    df = run_data(str(tmp_path))
    assert isinstance(df, pd.DataFrame)
    assert not df.empty
    assert list(df.columns) == [
        "Target", "Course", "Marital status", "Mother's occupation", 
        "Father's occupation", "Var1", "Var2"
    ]

def test_run_data_missing_file(tmp_path):
    """Test `run_data` handles missing file gracefully."""
    df = run_data(str(tmp_path))
    assert df is None

def test_select_multiples_var(mock_dataframe):
    """Test `select_multiples_var` identifies multiple modality columns."""
    columns_mult = select_multiples_var(mock_dataframe)
    assert list(columns_mult) == ["Course", "Marital status", "Mother's occupation", "Father's occupation",'Var1','Var2']

def test_calculate_percent(mock_dataframe):
    """Test `calculate_percent` calculates percentages correctly."""
    percent = calculate_percent(mock_dataframe, "Target")
    assert isinstance(percent, pd.Series)
    assert percent.to_dict() == {1: 50.0, 0: 50.0}

def test_calculate_percent_invalid_column(mock_dataframe):
    """Test `calculate_percent` raises error for invalid column."""
    with pytest.raises(KeyError):
        calculate_percent(mock_dataframe, "Invalid Column")

def test_check_na(mock_dataframe, capsys):
    """Test `check_na` outputs correct NA summary."""
    check_na(mock_dataframe)
    captured = capsys.readouterr()
    assert "0" in captured.out  # No missing values in mock data

def test_check_types(mock_dataframe, capsys):
    """Test `check_types` outputs correct data types."""
    check_types(mock_dataframe)
    captured = capsys.readouterr()
    assert "int64" in captured.out
    assert "float64" in captured.out

def test_display_dataset(mock_dataframe, capsys):
    """Test `display_dataset` outputs first rows correctly."""
    display_dataset(mock_dataframe)
    captured = capsys.readouterr()
    assert "Course" in captured.out

def test_display_desc_stat(mock_dataframe, capsys):
    """Test `display_desc_stat` outputs descriptive statistics."""
    display_desc_stat(mock_dataframe)
    captured = capsys.readouterr()
    assert "count" in captured.out
    assert "mean" in captured.out

def test_display_pie_chart(mock_dataframe, monkeypatch):
    """Test `display_pie_chart` displays chart without errors."""
    def mock_show():
        pass
    monkeypatch.setattr(plt, "show", mock_show)
    
    display_pie_chart(mock_dataframe, "Target", "Distribution of Target")

def test_display_pie_chart_invalid_column(mock_dataframe):
    """Test `display_pie_chart` raises error for invalid column."""
    with pytest.raises(ValueError):
        display_pie_chart(mock_dataframe, "Invalid Column", "Invalid Pie Chart")

def test_display_barplot(mock_dataframe, monkeypatch):
    """Test `display_barplot` displays chart without errors."""
    def mock_show():
        pass
    monkeypatch.setattr(plt, "show", mock_show)
    
    display_barplot(mock_dataframe, "Course", "Distribution by Course", "Course", "Count", 1, 17)

def test_display_barplot_invalid_column(mock_dataframe):
    """Test `display_barplot` raises error for invalid column."""
    with pytest.raises(ValueError):
        display_barplot(mock_dataframe, "Invalid Column", "Invalid Barplot", "X", "Y", 1, 10)

def test_display_boxplot(mock_dataframe, monkeypatch):
    """Test `display_boxplot` displays plots without errors."""
    def mock_show():
        pass
    monkeypatch.setattr(plt, "show", mock_show)
    
    display_boxplot(mock_dataframe)

def test_display_wordcloud(mock_dataframe, monkeypatch):
    """Test `display_wordcloud_` generates a wordcloud without errors."""
    def mock_show():
        pass
    monkeypatch.setattr(plt, "show", mock_show)
    
    mapping_course = {
        1: "Course A", 3: "Course B", 7: "Course C", 10: "Course D"
    }
    display_wordcloud_(mock_dataframe, "Course", mapping_course)
