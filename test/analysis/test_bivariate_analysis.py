import pytest
import pandas as pd
import numpy as np
import sys
import os
from io import StringIO
import matplotlib.pyplot as plt
sys.path.append(os.path.abspath(os.path.join(os.path.dirname(__file__), '../../script/analysis')))

from bivariate_analysis import (run_data, do_mapping, do_crosstab, check_na, check_types, 
                              calculate_desc_stat, display_barplot, display_kde, 
                              display_stacked_bars, display_countplot, display_boxplot, 
                              display_scatterplot)

@pytest.fixture
def sample_data():
    """
    Fixture pour créer un DataFrame d'exemple.
    """
    return pd.DataFrame({
        'Gender': [0, 1, 0],
        'Age at enrollment': [18, 20, 22],
        'Course': [1, 2, 1],
        'Tuition fees up to date': [1, 0, 1],
        'Scholarship holder': [0, 1, 0],
        'Curricular units 2nd sem (enrolled)': [5, 6, 4],
        'Curricular units 2nd sem (evaluations)': [4, 5, 3]
    })

@pytest.fixture
def mappings():
    """
    Fixture pour générer les mappings.
    """
    return do_mapping()

def test_run_data():
    """
    Test de la fonction `run_data`.
    """
    df = run_data("non_existing_path/")
    assert df is None, "La fonction devrait renvoyer None si le fichier n'existe pas."

def test_do_mapping(sample_data, mappings):
    """
    Test de la fonction `do_mapping`.
    """
    mapped_data = sample_data.replace(mappings)
    assert mapped_data.loc[0, 'Gender'] == "Female", "Le mapping de Gender a échoué."
    assert mapped_data.loc[1, 'Course'] == "Animation and Multimedia Design", "Le mapping de Course a échoué."

def test_do_crosstab(sample_data):
    """
    Test de la fonction `do_crosstab`.
    """
    cross_tab = do_crosstab(sample_data, 'Gender', 'Course')
    assert cross_tab.loc[0, 1] == 2, "Le calcul du crosstab est incorrect."

def test_check_na(sample_data, capsys):
    """
    Test de la fonction `check_na`.
    """
    check_na(sample_data)
    captured = capsys.readouterr()
    assert "Gender" in captured.out, "Les colonnes avec NA ne sont pas correctement signalées."

def test_check_types(sample_data, capsys):
    """
    Test de la fonction `check_types`.
    """
    check_types(sample_data)
    captured = capsys.readouterr()
    assert "Age at enrollment" in captured.out, "Les types de données ne sont pas correctement affichés."

def test_calculate_desc_stat(sample_data):
    """
    Test de la fonction `calculate_desc_stat`.
    """
    stats = calculate_desc_stat(sample_data, "Gender", "Age at enrollment")
    assert pytest.approx(stats.loc[0, 'mean'], 0.1) == 20, "Le calcul des statistiques descriptives est incorrect."

def test_display_barplot(sample_data):
    """
    Test si `display_barplot` génère un graphique sans erreurs.
    """
    try:
        display_barplot(sample_data, "Course", "Age at enrollment", "Gender")
    except Exception as e:
        pytest.fail(f"display_barplot a levé une exception : {e}")

def test_display_kde(sample_data):
    """
    Test si `display_kde` génère un graphique sans erreurs.
    """
    try:
        display_kde(sample_data, "Age at enrollment", "Gender")
    except Exception as e:
        pytest.fail(f"display_kde a levé une exception : {e}")

def test_display_stacked_bars(sample_data):
    """
    Test de la fonction `display_stacked_bars`.
    """
    crosstab = do_crosstab(sample_data, "Course", "Tuition fees up to date")
    try:
        display_stacked_bars(crosstab)
    except Exception as e:
        pytest.fail(f"display_stacked_bars a levé une exception : {e}")

def test_display_scatterplot(sample_data):
    """
    Test de la fonction `display_scatterplot`.
    """
    try:
        display_scatterplot(sample_data, "Age at enrollment", "Curricular units 2nd sem (enrolled)", "Gender")
    except Exception as e:
        pytest.fail(f"display_scatterplot a levé une exception : {e}")
