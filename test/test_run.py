import pytest
import sys
from io import StringIO
import sys
import os
sys.path.append(os.path.abspath(os.path.join(os.path.dirname(__file__), '../script/')))
from modele_prediction import TargetEstimator
from run import collect_data, run

@pytest.fixture
def target_estimator():
    """
    Fixture pour créer une instance de TargetEstimator.
    """
    return TargetEstimator()

@pytest.fixture
def sample_user_input():
    """
    Fixture pour fournir un exemple d'entrée utilisateur.
    """
    return "1-2-3-1-1-5-6-20-2.5"  # Exemple d'entrée utilisateur sous format attendu.

def test_collect_data(sample_user_input):
    """
    Test de la fonction collect_data.
    """
    variables = collect_data(sample_user_input)
    
    expected_result = [
        [1, 2, 3, 1, 1, 5, 6, 20, 2.5]
    ]
    assert variables == expected_result, "La fonction collect_data ne renvoie pas les bonnes données."

def test_run(target_estimator, sample_user_input, monkeypatch):
    """
    Test de la fonction run.
    """
    # Simulation des arguments de ligne de commande
    test_args = ["main.py", sample_user_input]
    monkeypatch.setattr(sys, "argv", test_args)
    
    # Redirection de la sortie standard pour capturer les impressions
    captured_output = StringIO()
    monkeypatch.setattr(sys, "stdout", captured_output)

    # Mock des méthodes de TargetEstimator
    def mock_load_data():
        data = {
            'Marital status': [1],
            'Course': [2],
            "Mother's occupation": [3],
            'Gender': [1],
            'Tuition fees up to date': [1],
            'Curricular units 2nd sem (evaluations)': [5],
            'Curricular units 2nd sem (enrolled)': [6],
            'Age at enrollment': [20],
            'Inflation rate': [2.5],
            'Target': ['Graduate']
        }
        return pd.DataFrame(data)

    def mock_encode_variables(df):
        X = pd.DataFrame({
            'const': [1],
            'Curricular units 2nd sem (evaluations)': [0],
            'Curricular units 2nd sem (enrolled)': [0],
            'Age at enrollment': [0],
            'Inflation rate': [0],
            'Marital status_1': [1],
            'Course_2': [1],
            "Mother's occupation_3": [1],
            'Gender_1': [1],
            'Tuition fees up to date_1': [1]
        })
        y = pd.Series([2], name='Target', dtype="category")
        return X, y

    def mock_get_modele(X, y):
        return None  # Pas nécessaire pour ce test

    def mock_get_prediction(curr_2nd_eval, curr_2nd_enrolled, age, inflation, tuition, marital_status, 
                            mothers_occupation, gender, course):
        return [0.15, 0.3, 0.55]  # Simuler une prédiction

    # Mock les méthodes de TargetEstimator
     # Mock les méthodes de TargetEstimator
    target_estimator.load_data = mock_load_data
    target_estimator.encode_variables = mock_encode_variables
    target_estimator.get_modele = mock_get_modele
    target_estimator.get_prediction = mock_get_prediction

    # Exécuter le script principal
    run()

    # Capturer et tester la sortie
    captured_output.seek(0)
    output = captured_output.read().strip()

    assert "Graduate" in output, "La prédiction attendue 'Graduate' n'est pas dans la sortie."
