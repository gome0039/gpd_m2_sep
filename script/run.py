"""
Main script for estimating academic outcomes using the Logistic Regression model.
"""
import sys
from modele_prediction import TargetEstimator
def collect_data(phrase):
    """
    This function takes a sentence as input and splits it into a list of words.
    
    Parameters:
    - phrase: A string to be split into words
    
    Returns:
    - A DataFrame containing the sentence data
    """
    mots = phrase.split('-') 
    # Variables de la première ligne du code
    curr_2nd_eval = int(mots[0])
    curr_2nd_enrolled = int(mots[1])
    curr_2nd_approved = int(mots[2])
    curr_2nd_grade = int(mots[3])
    age = int(mots[4])
    istuition = int(mots[5])
    isgender = int(mots[6])
    isscholarship_holder = int(mots[7])


    variables = [
        
            curr_2nd_eval,
            curr_2nd_enrolled,
            curr_2nd_approved,
            curr_2nd_grade,
            age,
            istuition,
            isgender,
            isscholarship_holder,
        
    ]
    
    return variables
def run():
    """
    Main function for script execution.
    Loads data, preprocesses, trains the model, and estimates academic outcomes based on command-line arguments.
    Prints the result.
    """
    target_estimator = TargetEstimator()
    
    # Load data
    #df = target_estimator.load_data()
    
    # Preprocess data
    #X, y = target_estimator.encode_variables(df)
    
    # Train model
    #target_estimator.get_modele(X, y)
   
    # Print the prediction result
    user_input = sys.argv[1] 
    df = collect_data(user_input)

    y_pred = target_estimator.get_prediction(
        df[0],  # curr_2nd_eval
        df[1],  # curr_2nd_enrolled
        df[2],  # curr_2nd_approved
        df[3],  # curr_2nd_grade
        df[4],  # age
        df[5],  # istuition
        df[6],  # isgender
        df[7],  # isscholarship_holder
    )
  
    if max(y_pred)==y_pred[0]:
        print('Dropout')
        print(round(y_pred[0],2))
    # elif max(y_pred)==y_pred[1]:
    #     print('Enrolled')
    #     print(round(y_pred[1],2))
    elif max(y_pred)==y_pred[1]:
        print('Graduate')
        print(round(y_pred[1],2))
