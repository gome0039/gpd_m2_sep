import os
import sys
# Ajouter le répertoire 'config' et le répertoire parent au sys.path
sys.path.append(os.path.abspath(os.path.join(os.path.dirname(__file__), '..', '..', 'config')))
sys.path.append(os.path.abspath(os.path.join(os.path.dirname(__file__), '..', '..')))

# Importation of necessary libraries
import matplotlib.pyplot as plt
import seaborn as sns
import pandas as pd
from sklearn.model_selection import cross_val_score
from sklearn.metrics import classification_report, confusion_matrix, accuracy_score
from sklearn.ensemble import RandomForestClassifier
from config.constants import MAPPING_Y
from typing import Tuple
from sklearn.metrics import classification_report, confusion_matrix, accuracy_score
import numpy as np


def train_random_forest(X_train: pd.DataFrame, y_train: pd.DataFrame) -> RandomForestClassifier:
    """
    Trains a Random Forest classifier on the given training data.

    Parameters:
    - X_train : pd.DataFrame : Independent variables for training data.
    - y_train : pd.DataFrame : Target variable for training.

    Returns:
    - random_forest : RandomForestClassifier : The trained Random Forest model.
    """
    random_forest = RandomForestClassifier(n_estimators=500, random_state=42)
    random_forest.fit(X_train, y_train)
    return random_forest

def find_optimal_n_estimators(X_train: pd.DataFrame, y_train: pd.Series, X_test: pd.DataFrame, y_test: pd.Series ) -> Tuple[int, RandomForestClassifier]:
    """
    Identifies the optimal number of trees for a Random Forest model using cross-validation
    and trains a final model with the optimal number of trees.

    Parameters:
    ----------
    X_train : pd.DataFrame
        Training data (features).
    y_train : pd.Series
        Labels associated with the training data.
    X_test : pd.DataFrame
        Test data (features).
    y_test : pd.Series
        Labels associated with the test data.

    Returns:
    -------
    Tuple[int, RandomForestClassifier]
        A tuple containing:
        - The optimal number of trees.
        - The trained Random Forest model with the optimal number of trees.
    """
    # List of n_estimators to test
    n_estimators_list = [100, 150, 200, 300, 400, 500, 550, 600, 700, 900, 1000]

    # List to store cross-validation performance scores
    accuracy_scores = []

    # Cross-validation for each n_estimators value
    for n_estimators in n_estimators_list:
        model = RandomForestClassifier(n_estimators=n_estimators, random_state=42)
        scores = cross_val_score(model, X_train, y_train, cv=5, scoring='accuracy')
        accuracy_scores.append(scores.mean())

    # Identify the optimal number of trees
    optimal_n_estimators = n_estimators_list[np.argmax(accuracy_scores)]
    print(f"Le nombre optimal d'arbres est : {optimal_n_estimators}")

    # Plot the performance graph
    plt.figure(figsize=(8, 6))
    plt.plot(n_estimators_list, accuracy_scores, marker='o', linestyle='-', color='blue')
    plt.xlabel("Nombre d'arbres (n_estimators)")
    plt.ylabel("Précision moyenne (Accuracy)")
    plt.title("Performance en fonction du nombre d'arbres")
    plt.grid(True)
    plt.show()

    # Train the final model with the optimal number of trees
    final_model = RandomForestClassifier(n_estimators=optimal_n_estimators, random_state=42)
    final_model.fit(X_train, y_train)

    # Predictions on the test set
    y_pred = final_model.predict(X_test)
    final_accuracy = accuracy_score(y_test, y_pred)
    print(f"Précision sur le jeu de test avec {optimal_n_estimators} arbres : {final_accuracy:.4f}")

    return optimal_n_estimators, final_model

def evaluate_model(X_test: pd.DataFrame, y_test: pd.DataFrame, model) -> None:
    """
    Evaluates a given classifier model on test data and prints the classification report.

    Parameters:
    - X_test : pd.DataFrame : Independent variables for test data.
    - y_test : pd.DataFrame : Target variable for test data.
    - model : Any : The trained classifier model.
    """
    y_pred = model.predict(X_test)
    cm = confusion_matrix(y_test, y_pred)

    # Print classification report
    print(f"{model.__class__.__name__} Classification Report:\n", classification_report(y_test, y_pred))
    
    # Calculate the confusion matrix
    cm = confusion_matrix(y_test, y_pred)

    # Plot confusion matrix as a heatmap
    plt.figure(figsize=(8, 6))
    sns.heatmap(cm, annot=True, fmt='d', cmap='Blues', xticklabels=MAPPING_Y, yticklabels=MAPPING_Y)
    plt.xlabel("Classes prédites")
    plt.ylabel("Classes réelles")
    plt.xticks(fontsize=7)  
    plt.yticks(fontsize=7) 
    plt.title("Matrice de confusion du RandomForest")
    plt.show()

def graph_important_features(rf_model, X_train: pd.DataFrame, crit: float) -> None:
    """
    Plots a bar chart for the important features in a Random Forest model.

    Parameters:
    - rf_model : RandomForestClassifier : The trained Random Forest model.
    - X_train : pd.DataFrame : The training data used to train the model.
    - crit : float : The threshold criterion for selecting important features.
    """
  
    # Get feature importances
    feature_importances = rf_model.feature_importances_
    
    # Create a DataFrame with features and their importance scores
    feature_df = pd.DataFrame({
        'Feature': X_train.columns,
        'Importance': feature_importances
    })
    
    # Filter features that are above the threshold criterion
    important_features = feature_df[feature_df['Importance'] >= crit]
    
    # Sort the features by importance
    important_features = important_features.sort_values(by='Importance', ascending=False)
    print(important_features)
    
    # Plotting
    plt.figure(figsize=(8, 4))
    sns.barplot(
        x="Importance", 
        y="Feature", 
        data=important_features,
        color="#64A349"
    )
    plt.title(f"Features importantes dans RandomForest avec une importance >= {crit}")
    plt.xlabel("Importance des features")
    plt.ylabel("Features")

    # Adjusting label font size for features
    plt.yticks(fontsize=6, rotation=45)  # Rotate labels at 45 degrees for better readability

    plt.show()
