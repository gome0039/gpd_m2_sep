import os
import sys
# Ajouter le répertoire 'config' et le répertoire parent au sys.path
sys.path.append(os.path.abspath(os.path.join(os.path.dirname(__file__), '..', '..', 'config')))
sys.path.append(os.path.abspath(os.path.join(os.path.dirname(__file__), '..', '..')))
# Importation of necessary libraries
import pandas as pd
from tools_randomforest import train_random_forest, evaluate_model,graph_important_features,find_optimal_n_estimators
from config.constants import X_TEST, X_TRAIN ,Y_TRAIN, Y_TEST

if __name__=="__main__":

    path = os.path.join(os.path.dirname(os.path.abspath(__file__)),"..","data_train_test")
    X_train = pd.read_csv(os.path.join(path, X_TRAIN))
    y_train = pd.read_csv(os.path.join(path, Y_TRAIN)).values.ravel()
    
    X_test = pd.read_csv(os.path.join(path, X_TEST))
    y_test = pd.read_csv(os.path.join(path, Y_TEST)).values.ravel()

    # Train the Random Forest model using the training data
    random_forest = train_random_forest(X_train, y_train)

    # Evaluate the trained model on the test data
    evaluate_model(X_test, y_test, random_forest)

    # Find the optimal number of estimators (trees) for the Random Forest model by testing different values
    find_optimal_n_estimators(X_train,y_train,X_test,y_test)

    # Display the important features based on a threshold importance criterion 
    graph_important_features(random_forest, X_train, crit=0.01)
