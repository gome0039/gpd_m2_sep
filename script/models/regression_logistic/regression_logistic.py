import os
import sys
# Ajouter le répertoire 'config' et le répertoire parent au sys.path
sys.path.append(os.path.abspath(os.path.join(os.path.dirname(__file__), '..', '..', 'config')))
sys.path.append(os.path.abspath(os.path.join(os.path.dirname(__file__), '..', '..')))

from tools_regressionlogistic import  LogRegression_with_lasso,coefRL, evaluate_model,best_estimator, extract_zero_coefficients, RL,vif,LogRegression_withLassoCV, kfolds_error
import matplotlib.pyplot as plt
import pandas as pd
import statsmodels.api as sm
from sklearn.linear_model import LogisticRegression
from sklearn.model_selection import train_test_split
from sklearn.metrics import classification_report, confusion_matrix, accuracy_score
from sklearn.preprocessing import StandardScaler
from statsmodels.stats.outliers_influence import variance_inflation_factor
from pathlib import Path
import os
from scipy.stats import chi2
from modele_prediction import TargetEstimator
from config.constants import X_TEST,X_TRAIN, Y_TEST, Y_TRAIN
from typing import Tuple
import numpy as np


if __name__=="__main__":

    path = os.path.join(os.path.dirname(os.path.abspath(__file__)),"..","data_train_test")
    
    X_encoded = pd.read_csv(os.path.join(path, "data_encode.csv"))
    X_train = pd.read_csv(os.path.join(path, X_TRAIN))
    y_train = pd.read_csv(os.path.join(path, Y_TRAIN)).values.ravel()
    
    X_test = pd.read_csv(os.path.join(path, X_TEST))
    y_test = pd.read_csv(os.path.join(path, Y_TEST)).values.ravel()
    

    # Step 1: Logistic Regression without regularization

    # Train the logistic regression model without Lasso (regularization)
    model_without_lasso = RL(X_train, y_train)

    # Display the coefficients of the logistic regression model
    coefRL(model_without_lasso, X_train.columns)

    # Evaluate the model's performance on the test set
    evaluate_model(X_test, y_test, model_without_lasso)

    # Print the dimensions of the training set to understand the feature space
    print(f"Dimension of the training set: {X_train.shape}")

    # Step 2: Logistic Regression
    # ======================================================================

    # Train a Logistic Regression model with LASSO regularization
    logistic_model = LogRegression_with_lasso(X_train, y_train)

    # Evaluate the model's performance on the test dataset
    evaluate_model(X_test, y_test, logistic_model)

    # Display the coefficients of the Logistic Regression model
    coefRL(logistic_model, X_train.columns)

    # Check Variance Inflation Factors (VIF) to detect multicollinearity (optional)
    # print("VIF = ", vif(X_train))

    # Cross-validation for the Logistic Regression model (optional)
    # validate_logistic_regression(X_train, y_train, logistic_model)

    # Extract variables whose coefficients are set to zero by LASSO
    # variables_to_drop = extract_zero_coefficients(logistic_model, X_train.columns)
    # print(f"Number of variables set to zero: {len(variables_to_drop)}")
    # print(f"Variables set to zero: {variables_to_drop}")

    # Drop the unused columns from the dataset after feature selection
    # X_encoded = X_encoded.drop(columns=[col for col in variables_to_drop if col in X_encoded.columns])

    # Split the data with the remaining variables
    # X_train, X_test, y_train, y_test = train_test_split(X_encoded, y, test_size=0.2, random_state=42, stratify=y)

    # Step 3: Fit and summarize a Logistic Regression model with statsmodels
    # ======================================================================

    # Replace target variable value `2` with `1` (if applicable)
    y_train[y_train == 2] = 1

    # Build and fit the logistic regression model using statsmodels
    logit_model = sm.Logit(y_train, X_train)
    result = logit_model.fit()

    # Display the summary of the fitted model
    print(result.summary())
