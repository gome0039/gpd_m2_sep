# Importation of necessary libraries
import matplotlib.pyplot as plt
import seaborn as sns
import pandas as pd
import numpy as np
import statsmodels.api as sm
from sklearn.linear_model import LogisticRegression,LogisticRegressionCV
from sklearn.model_selection import  GridSearchCV
from sklearn.metrics import classification_report, confusion_matrix
from statsmodels.stats.outliers_influence import variance_inflation_factor
from config.constants import MAPPING_Y
from sklearn.model_selection import cross_val_score, KFold


def LogRegression_with_lasso(X_train: pd.DataFrame, y_train: pd.DataFrame) -> LogisticRegression:
    """
    Trains a logistic regression model.

    Parameters:
    - X_train : pd.DataFrame : Independent variables for training data.
    - y_train : pd.DataFrame : Target variable for training.

    Returns:
    - logistic_model : LogisticRegression : The trained logistic regression model.
    """
    logistic_model = LogisticRegression(
        penalty='l1',            # L1 regularization
        solver='liblinear',           # Solver suitable for L1
        C=1,                   # Inverse of regularization strength
        max_iter=1000, 
        fit_intercept=True,
        random_state=42
    )
    
    logistic_model.fit(X_train, y_train)
    print("Nombre d'itérations pour chaque classe :", logistic_model.n_iter_)
   
    return logistic_model

def kfolds_error(X: pd.DataFrame,y : pd.DataFrame):
    """
    Perform k-fold cross-validation on a logistic regression model with L1 regularization and compute the classification error.

    Parameters:
    ----------
    X : pd.DataFrame
        The feature matrix containing the predictors.
    y : pd.DataFrame
        The target variable (labels) associated with the data.

    Description:
    -----------
    This function evaluates the performance of a logistic regression model using k-fold cross-validation. 
    It applies L1 regularization (Lasso) for feature selection and uses the 'liblinear' solver for optimization.
    The model's classification error is calculated and the accuracy scores for each fold are visualized.

    Returns:
    -------
    None

    Outputs:
    -------
    - Prints the accuracy scores for each fold.
    - Prints the mean accuracy and classification error.
    - Generates a plot showing the accuracy scores across all folds.

    Notes:
    ------
    - The `plot_kfolds` function is assumed to generate a plot of accuracy scores and their mean.
    """
    model = LogisticRegression(
        penalty='l1',            # L1 regularization
        solver='liblinear',      # Solver suitable for L1
        C=1.2,                   # Inverse of regularization strength
        max_iter=1000, 
        fit_intercept=False
    )    
    # K-fold cross-validation configuration
    kfold = KFold(n_splits=50, shuffle=True, random_state=42)

    # Evaluate the accuracy for each fold
    scores = cross_val_score(model, X, y, cv=kfold, scoring='accuracy')

    # Calculate the average classification error
    average_accuracy = scores.mean()
    classification_error = 1 - average_accuracy

    print("Scores d'exactitude pour chaque pli :", scores)
    print("Précision moyenne :", average_accuracy)
    print("Erreur de classification moyenne :", classification_error)
    plot_kfolds(scores,average_accuracy)

   

def plot_kfolds(scores, average_accuracy):
    """
    Plots the accuracy scores for each fold in k-fold cross-validation and shows the average accuracy.

    Parameters:
    ----------
    scores : list or array-like
        The list of accuracy scores for each fold in the k-fold cross-validation.
    average_accuracy : float
        The mean accuracy across all folds.

    Description:
    -----------
    This function generates a plot to visualize the accuracy scores for each fold of k-fold cross-validation. 
    It also displays a horizontal line representing the average accuracy across all folds to give a sense of the overall model performance.

    Returns:
    -------
    None

    Outputs:
    -------
    - Displays a plot with the accuracy scores for each fold and the average accuracy.
    """
 
    plt.figure(figsize=(8, 6))
    plt.plot(range(1, len(scores) + 1), scores, marker='o', linestyle='-', color='b', label="Accuracy par partition")
    plt.axhline(y=average_accuracy, color='r', linestyle='--', label=f'Précision moyenne = {average_accuracy:.3f}')
    plt.title("Scores de précision pour chaque partition de la validation croisée")
    plt.xlabel("Partition")
    plt.ylabel("Précision")
    plt.legend()
    plt.grid(True)
    plt.show()

def plot_lassocv(lasso_cv: LogisticRegressionCV):
    """
    Displays the classification error based on different values of C 
    after training the model.

    Parameters:
    - lasso_cv : LogisticRegressionCV : The trained model.

    This function plots the classification error as a function of the regularization strength (C),
    using a logarithmic scale for C.
    """
    # Extracting cross-validation scores for each value of C
    mean_scores = np.mean(lasso_cv.scores_[2], axis=0)  # Average scores for each C
  
    # Calculating classification errors for each value of C
    classification_errors = 1 - mean_scores

    # Displaying classification errors based on C
    plt.plot(lasso_cv.Cs_, classification_errors, marker='o')
    plt.xscale('log')  # logarithmic scale for C
    plt.xlabel('Valeur de C (échelle logarithmique)')
    plt.ylabel('Erreur de classification')
    plt.title('Erreur de classification en fonction de C')
    plt.show()

def LogRegression_withLassoCV(X_train: pd.DataFrame, y_train: pd.DataFrame) -> LogisticRegression:
    """
    Trains a logistic regression model.

    Parameters:
    - X_train : pd.DataFrame : Independent variables for training data.
    - y_train : pd.DataFrame : Target variable for training.

    Returns:
    - logistic_model : LogisticRegression : The trained logistic regression model.
    """
    lasso_cv = LogisticRegressionCV(
        Cs= 50,                     # Number of C values to test
        cv=50,                      # Number of cross-validation folds
        penalty='l1',              # Type of penalty: L1 for LASSO
        solver='liblinear',             # Solver compatible with L1
        max_iter=1000,            # Increase iterations to ensure convergence
        scoring='accuracy',        # Metric to optimize
        fit_intercept=True
    )
    lasso_cv.fit(X_train, y_train)
    print("Best value of C selected by cross-validation:", lasso_cv.C_)
    
    # Call the function to display the classification error
    plot_lassocv(lasso_cv)
    return lasso_cv


def vif(X_train: pd.DataFrame) -> pd.DataFrame:
    """
    Calculates the Variance Inflation Factor (VIF) for each feature.

    Parameters:
    - X_train : pd.DataFrame : The input dataframe with independent variables.

    Returns:
    - vif : pd.DataFrame : Dataframe with VIF values for each variable.
    """
    vif = pd.DataFrame()
    vif["Variable"] = X_train.columns
    vif["VIF"] = [variance_inflation_factor(X_train.values, i).round(2) for i in range(X_train.shape[1])]
    return vif


def extract_zero_coefficients(model: LogisticRegression, columns: pd.Index) -> pd.DataFrame:
    """
    Extracts variables whose coefficients are zero for all classes in the logistic regression model.

    Parameters:
    - model : LogisticRegression : The trained logistic regression model.
    - columns : pd.Index : The feature names (columns) corresponding to the model's coefficients.

    Returns:
    - variables : pd.Index : Index of variables that have a coefficient of zero across all classes.
    """
    # Create a DataFrame with coefficients, using the feature names as index
    if len(model.classes_) == 2:  # Binary classification
        coefficients = pd.DataFrame(
            model.coef_.T,
            index=columns,
            columns=[f"Coef for {model.classes_[1]}"]
        )
    else:  # Multinomial classification
        coefficients = pd.DataFrame(
            model.coef_.T,
            index=columns,
            columns=model.classes_
        )

    
    # Filter variables where the coefficient is zero for all classes
    zero_coefficients = coefficients[(coefficients == 0).all(axis=1)]
    
    # Extract the index (variable names) of coefficients that are zero for all classes
    variables = zero_coefficients.index.tolist()
    
    # Return the list of variables
    return variables


def graph_larg_coeff(largest_coefficients: pd.DataFrame, crit: float) -> None:
    """
    Plots a bar chart for the largest coefficients.

    Parameters:
    - largest_coefficients : pd.DataFrame : DataFrame containing the largest coefficients.
    - crit : float : The threshold criterion.
    """
    plt.figure(figsize=(8, 6))
    sns.barplot(
        x="Coefficient", 
        y="index", 
        hue="Classe", 
        data=largest_coefficients, 
        dodge=True
    )
    plt.yticks(fontsize=5)
    plt.axvline(0, color='black', linewidth=0.5)
    plt.title(f"Forest Plot of Important Coefficients with a criterion of {crit}")
    plt.ylabel("Variables")
    plt.xlabel("Coefficient")
    plt.show()


def coefRL(model: LogisticRegression, columns: pd.Index) -> None:
    """
    Prints the coefficients of the logistic regression model for each class.

    Parameters:
    - model : LogisticRegression : Trained logistic regression model.
    - columns : pd.Index : The columns of the features.
    """

    if len(model.classes_) == 2:  # Binary classification
        coefficients = pd.DataFrame(
            model.coef_.T,
            index=columns,
            columns=[f"Coef for {model.classes_[1]}"]
        )
    else:  # Multinomial classification
        coefficients = pd.DataFrame(
            model.coef_.T,
            index=columns,
            columns=model.classes_
        )
    print("Coefficients for each class:")
    print(coefficients)


def RL(X_train: pd.DataFrame, y_train: pd.DataFrame) -> sm.MNLogit:
    """
    Trains a multinomial logistic regression model using statsmodels.

    Parameters:
    - X_train : pd.DataFrame : Independent variables for training data.
    - y_train : pd.DataFrame : Target variable for training.

    Returns:
    - logit_model : LogisticRegression : Trained multinomial logistic regression model.
    """
    logit_model = LogisticRegression(penalty = None,
                                    solver='lbfgs', 
                                    fit_intercept=True,
                                    max_iter = 1000,
                                    random_state=0
                                   )
    logit_model.fit( X_train,y_train)
    print("Nombre d'itérations pour chaque classe :", logit_model.n_iter_)

    return logit_model

def best_estimator(X_train, y_train):
    parameters = {'C': [0.01, 0.1, 0.06, 1, 10], 'solver' : ['liblinear', 'saga']}
    logistic = LogisticRegression(
                                penalty='l1',
                                solver='liblinear', 
                                fit_intercept=True,
                                max_iter = 1000,
                                random_state=0)
    scikit_GridSearchCV = GridSearchCV(logistic, parameters)
    scikit_GridSearchCV.fit(X_train,y_train)
    print(f"Best estimator: {scikit_GridSearchCV.best_estimator_}")
    

def evaluate_model(X_test: pd.DataFrame, y_test: pd.DataFrame, model) -> None:
    """
    Evaluates a given classifier model on test data, prints the classification report, and displays the confusion matrix.

    Parameters:
    ----------
    X_test : pd.DataFrame
        Independent variables for test data.
        
    y_test : pd.DataFrame
        Target variable for test data.
        
    model : Any
        The trained classifier model.
    
    Returns:
    -------
    None
        Prints the classification report and displays the confusion matrix.
    """
    # Predict the target variable
    y_pred = model.predict(X_test)

    # Print classification report
    print(f"{model.__class__.__name__} Classification Report:\n", classification_report(y_test, y_pred))

    # Calculate the confusion matrix
    cm = confusion_matrix(y_test, y_pred)

    # Plot confusion matrix as a heatmap
    plt.figure(figsize=(8, 6))
    sns.heatmap(cm, annot=True, fmt='d', cmap='Blues', xticklabels=MAPPING_Y, yticklabels=MAPPING_Y)
    plt.xlabel("Classes prédites")
    plt.ylabel("Classes réelles")
    plt.xticks(fontsize=7)  
    plt.yticks(fontsize=7) 
    plt.title("Matrice de confusion de la regression logistique")
    plt.show()
