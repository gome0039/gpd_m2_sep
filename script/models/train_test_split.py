import os
import sys
# Add the 'config' directory and parent directory to the sys.path

sys.path.append(os.path.abspath(os.path.join(os.path.dirname(__file__), '..', 'config')))
sys.path.append(os.path.abspath(os.path.join(os.path.dirname(__file__), '..')))

# Importation of necessary libraries
from sklearn.model_selection import train_test_split
from modele_prediction import TargetEstimator
from config.constants import VAR_PRED, QUANT_VARS, QUAL_VARS, MAPPING_Y



if __name__=="__main__":
    target_estimator = TargetEstimator()
    df = target_estimator.load_data()

    df = df[VAR_PRED + QUANT_VARS + QUAL_VARS]
    df['Target'] = df['Target'].replace('Enrolled', 'Dropout')

    X_encoded, y = target_estimator.encode_variables(df)

    X_train, X_test, y_train, y_test = train_test_split(X_encoded, y, test_size=0.2, random_state=42,stratify=y)

    path = os.path.join(os.path.dirname(os.path.abspath(__file__)),"data_train_test")

    os.makedirs(path, exist_ok=True)

    X_encoded.to_csv(os.path.join(path, "data_encode.csv"), index=False)
    X_train.to_csv(os.path.join(path, "X_train_binaire.csv"), index=False)
    y_train.to_csv(os.path.join(path, "y_train_binaire.csv"), index=False)

    X_test.to_csv(os.path.join(path, "X_test_binaire.csv"), index=False)
    y_test.to_csv(os.path.join(path, "y_test_binaire.csv"), index=False)
