##########################################
####         Import libraries        #####
##########################################

import os
import pandas as pd 
import matplotlib.pyplot as plt
import seaborn as sns
import numpy as np
from wordcloud import WordCloud

##########################################
######          Functions           ######
##########################################

def run_data (path:str):
    """
    Parameter : 
        Path to access data set

    Function : 
        Run data set
    
    Return : 
        The dataframe database
    """
    
    df = None
    try :
        df = pd.read_csv(os.path.join(path,'dataset.csv'), delimiter = ",")
    except : 
        print ("File not found")
    return df

def select_multiples_var (dataframe:pd.DataFrame):
    """
    Parameter : 
        The dataframe database
    
    Function : 
        Select multiples modalities variables
    
    Return : 
        An Index variable containing the names of columns containing multiples modalities
    """
    
    columns_mult = dataframe.columns[dataframe.nunique() > 2]
    return columns_mult

def calculate_percent (dataframe:pd.DataFrame, column_name:str):
    """
    Parameter : 
        The dataframe database
    
    Function : 
        Calculate the percentage of a specified column
    
    Return : 
        Return the percentage of modalities in the specified column     
    """

    res = (dataframe[column_name].value_counts(normalize=True) * 100).round(2)
    return res

##########################################
######          Procedures          ######
##########################################

def check_na (dataframe:pd.DataFrame):
    """
    Parameter : 
        The dataframe database
    
    Function : 
        Check for missing values 
    """

    print(dataframe[select_multiples_var(dataframe)].isnull().sum())

def check_types(dataframe:pd.DataFrame):
    """
    Parameter : 
        The dataframe database
    
    Function : 
        Check data types
    """

    for col, dtype in dataframe.dtypes.items():
        print(f"{col}: {dtype}")


def display_dataset (dataframe:pd.DataFrame):
    """
    Parameter : 
        The dataframe database
    
    Function : 
        Display the first 5 lines of the dataset 
    """

    print(dataframe[select_multiples_var(dataframe)].head())

def display_desc_stat(dataframe:pd.DataFrame):
    """
    Parameter : 
        The dataframe database
    
    Function : 
        Statistical summary of multiple variable observations
    """   

    pd.set_option('display.max_columns',None)
    print(dataframe[select_multiples_var(dataframe)].describe(include='all'))
    pd.reset_option('display.max_columns')

def display_pie_chart(dataframe:pd.DataFrame, column_name:str, title:str):
    """
    Parameter : 
        - dataframe : The dataframe database
        - column_name : colum name which you want create a chart
        - title : chart title
        
    Function : 
        Display a circular graph
    """   

    if column_name not in dataframe.columns:
        raise ValueError(f"Column '{column_name}' not found in the DataFrame.")
    
    part = dataframe[column_name].value_counts()
    if part.empty:
        raise ValueError(f"Column '{column_name}' has no data to display in the pie chart.")

    plt.pie(part, labels=part.index, autopct='%1.1f%%', colors = sns.color_palette('coolwarm'))
    plt.title(title)
    plt.show()

def display_barplot(dataframe:pd.DataFrame, column_name:str, title:str, label_x:str, label_y:str, a:int, b:int):
    """
    Parameter : 
        - dataframe : The dataframe database
        - column_name : colum name which you want create a chart
        - title : chart title
        - label_x : name of abscissa
        - label_y : name of ordinate
        - a : start of abscissa
        - b : end of abscissa
        
    Function : 
        Display a bar graph
    """   
    
    if column_name not in dataframe.columns:
        raise ValueError(f"Column '{column_name}' not found in the DataFrame.")
    
    part = dataframe[column_name].value_counts()
    if part.empty:
        raise ValueError(f"Column '{column_name}' has no data to display in the barplot.")

    plt.bar(part.index, part.values)
    plt.title(title)
    plt.xlabel(label_x)
    plt.ylabel(label_y)
    plt.xticks(np.arange(a, b, 2))
    plt.show()

def display_boxplot(dataframe:pd.DataFrame):
    """
    Parameter : 
        - dataframe : The dataframe database
        
    Function : 
        Display a boxplot
    """ 

    for var in dataframe.select_dtypes(include='number').columns:
        sns.boxplot(dataframe[var],color='skyblue')
        plt.title(f"Boxplot of '{var}' variable")
        plt.tight_layout()
        plt.grid(False)
        plt.show()
        
def display_wordcloud_(dataframe:pd.DataFrame,column_name:str, mapping:str):
    """
    Parameter : 
        - dataframe : The dataframe database
        
    Function : 
        Display a wordcloud
    """ 

    dataframe[column_name] = dataframe[column_name].map(mapping)
    counts = dataframe[column_name].value_counts()
    wordcloud = WordCloud(
        background_color = 'white', 
        colormap = 'winter'
    ).generate_from_frequencies(counts)
    plt.figure(figsize=(10, 6))
    plt.imshow(wordcloud, interpolation='bilinear')
    plt.axis('off')
    plt.show()
    
##########################################
######             Main             ######
##########################################

if __name__ == "__main__":
    # Data import
    path = "../../data/"
    df = run_data(path)

    # Preparing and cleaning the database
    check_na(df)
    check_types(df)
    display_dataset(df)
    columns_mult = list(select_multiples_var(df))
    
    # Data manipulation
    df_var = df.iloc[:, [17,20,21,22,26,27,28,31,32,33]] # continuous variable recovery
    mapping_course = {
    1: 'Production de biocarburants',
    2: 'Animation et design multimédia',
    3: 'Service social (présence en soirée)',
    4: 'Agronomie',
    5: 'Design de la communication',
    6: 'Soins infirmiers vétérinaires',
    7: 'Génie informatique',
    8: 'Équiniculture',
    9: 'Gestion',
    10: 'Service social',
    11: 'Tourisme',
    12: 'Soins infirmiers',
    13: 'Hygiène bucco-dentaire',
    14: 'Gestion de la publicité et du marketing',
    15: 'Journalisme et communication',
    16: 'Éducation de base',
    17: 'Gestion (présence en soirée)'
    } # Correspondence dictionary

    df['Target'] = df['Target'].replace({'Graduate': 'Diplômé', 'Dropout': 'Décroché', 'Enrolled': 'Redoublé'})

    # Descriptives statistics
    display_desc_stat(df)
    percent_mother_occ = calculate_percent(df, "Mother's occupation")
    percent_father_occ = calculate_percent(df, "Father's occupation")
    percent_course = calculate_percent(df, 'Course')

    # Graphics display
    display_pie_chart(df, 'Target','Distribution by target variable')
    display_barplot(df, 'Course', 'Distribution by course','Course taken by students', 'Numbers',1,17)
    display_barplot(df, 'Marital status', 'Distribution by marital status','Marital status', 'Numbers',1,6)
    display_barplot(df, "Mother's occupation", "Distribution by mother's occupation", 'Occupation', 'Numbers', 1,46)
    display_boxplot(df_var)
    display_wordcloud_(df, 'Course', mapping_course)