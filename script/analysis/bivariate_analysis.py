##########################################
####         Import libraries        #####
##########################################

import os
import pandas as pd
import matplotlib.pyplot as plt
import seaborn as sns

##########################################
######          Functions           ######
##########################################

def run_data (path:str):
    """
    Parameter : 
        Path to access data set

    Function : 
        Run data set
    
    Return : 
        The dataframe database
    """
    
    df = None
    try :
        df = pd.read_csv(os.path.join(path,'dataset.csv'), delimiter = ",")
    except : 
        print ("File not found")
    return df

def do_mapping () :
    """
    Function : 
        Translate numerical codes into meaningful categorical labels
    
    Return : 
        A dictionary
    """  
    
    df_mappings = {
        'Marital status': {
            1: "Single", 
            2: "Married", 
            3: "Widower", 
            4: "Divorced", 
            5: "Facto Union", 
            6: "Legally Separated"
        },
        "Course": {
            1: "Biofuel Production Technologies", 
            2: "Animation and Multimedia Design", 
            3: "Social Service (evening attendance)", 
            4: "Agronomy", 
            5: "Communication Design", 
            6: "Veterinary Nursing", 
            7: "Informatics Engineering", 
            8: "Equinculture", 
            9: "Management", 
            10: "Social Service", 
            11: "Tourism", 
            12: "Nursing", 
            13: "Oral Hygiene", 
            14: "Advertising and Marketing Management", 
            15: "Journalism and Communication", 
            16: "Basic Education", 
            17: "Management (evening attendance)"
        },
        "Mother's occupation": {
            1: "Student", 
            2: "Representatives of the Legislative Power and Executive Bodies, Directors, Directors and Executive Managers", 
            3: "Specialists in Intellectual and Scientific Activities", 
            4: "Intermediate Level Technicians and Professions", 
            5: "Administrative staff", 
            6: "Personal Services, Security and Safety Workers and Sellers", 
            7: "Farmers and Skilled Workers in Agriculture, Fisheries and Forestry", 
            8: "Skilled Workers in Industry, Construction and Craftsmen", 
            9: "Installation and Machine Operators and Assembly Workers", 
            10: "Unskilled Workers", 
            11: "Armed Forces Professions", 
            12: "Other Situation", 
            13: "(blank)", 
            14: "Health professionals", 
            15: "Teachers", 
            16: "Specialists in information and communication technologies (ICT)", 
            17: "Intermediate level science and engineering technicians and professions", 
            18: "Technicians and professionals of intermediate level of health", 
            19: "Intermediate level technicians from legal, social, sports, cultural and similar services", 
            20: "Office workers, secretaries in general and data processing operators", 
            21: "Data, accounting, statistical, financial services and registry-related operators", 
            22: "Other administrative support staff", 
            23: "Personal service workers", 
            24: "Sellers", 
            25: "Personal care workers and the like", 
            26: "Skilled construction workers and the like, except electricians", 
            27: "Skilled workers in printing, precision instrument manufacturing, jewelers, artisans and the like", 
            28: "Workers in food processing, woodworking, clothing and other industries and crafts", 
            29: "Cleaning workers", 
            30: "Unskilled workers in agriculture, animal production, fisheries and forestry", 
            31: "Unskilled workers in extractive industry, construction, manufacturing and transport", 
            32: "Meal preparation assistants"
        },
        'Gender': {
            0: "Female", 
            1: "Male"
        },
        "Tuition fees up to date": {
            0: "No", 
            1: "Yes"
        },
        "Displaced": {
            0: "No", 
            1: "Yes"
        },
        "Scholarship holder": {
            0: "No", 
            1: "Yes"
        }
    }
    return df_mappings

def do_crosstab (dataframe:pd.DataFrame, column_name_1:str, column_name_2:str):
    """
    Parameter : 
        - dataframe : The dataframe database
        - column_name_1 : colum name number 1
        - column_name_2 : colum name number 2
    
    Function : 
        Generates a contingency table (cross-tabulation) to examine the relationship 
        between two categorical variables in the dataframe.
        
    Return : 
        A pandas DataFrame containing the cross-tabulation of the two columns.
    """   
   
    if not isinstance(dataframe, pd.DataFrame):
        raise ValueError("The provided input is not a valid DataFrame.")
    if column_name_1 not in dataframe.columns:
        raise ValueError(f"Column '{column_name_1}' not found in the DataFrame.")
    if column_name_2 not in dataframe.columns:
        raise ValueError(f"Column '{column_name_2}' not found in the DataFrame.")
        
    df_cross = pd.crosstab(dataframe[column_name_1], dataframe[column_name_2])
    return df_cross


##########################################
######          Procedures          ######
##########################################

def check_na (dataframe:pd.DataFrame):
    """
    Parameter : 
        The dataframe database
    
    Procedure : 
        Check for missing values
    """
    
    # Check if argument is a DataFrame
    if not isinstance(dataframe, pd.DataFrame):
        print("Error: The input is not a valid pandas DataFrame.")
        return
    # Check if DataFrame is empty
    if dataframe.empty:
        print("Error: The DataFrame is empty.")
        return
    
    print(dataframe.isnull().sum())

def check_types(dataframe:pd.DataFrame):
    """
    Parameter : 
        The dataframe database
    
    Procedure : 
        Check data types
    """

    if not isinstance(dataframe, pd.DataFrame):
        print("Error: The input is not a valid pandas DataFrame.")
        return
    if dataframe.empty:
        print("Error: The DataFrame is empty.")
        return    

    print(dataframe.dtypes)

def display_dataset(dataframe:pd.DataFrame):
    """
    Parameter : 
        The dataframe database
    
    Procedure : 
        Display the first 5 lines of the dataset 
    """

    if not isinstance(dataframe, pd.DataFrame):
        print("Error: The input is not a valid pandas DataFrame.")
        return
    if dataframe.empty:
        print("Error: The DataFrame is empty.")
        return    

    print(dataframe.head())

def calculate_desc_stat (dataframe: pd.DataFrame, group_column:str, target_column:str):
    """
    Parameters:
        - dataframe : The dataframe containing the data.
        - group_column : The column used for grouping (e.g., 'Gender').
        - target_column : The column to calculate statistics for (e.g., 'Curricular Units Enrolled').
    
    Procedure:
        Prints mean, median, and standard deviation for each group.
    """

    if not isinstance(dataframe, pd.DataFrame):
        raise TypeError("The provided object is not a valid pandas DataFrame.")
    if dataframe.empty:
        raise ValueError("The DataFrame is empty.")
    if group_column not in dataframe.columns or target_column not in dataframe.columns:
        raise ValueError(f"Column '{group_column}' or '{target_column}' not found in the DataFrame.")
    if not pd.api.types.is_numeric_dtype(dataframe[target_column]):
        raise ValueError(f"The target column '{target_column}' must be numeric.")
    if dataframe[[group_column, target_column]].isnull().any().any():
        raise ValueError(f"The columns '{group_column}' or '{target_column}' contain missing values.")
    

    stats = df.groupby(group_column)[target_column].agg(["mean", "median", "std"])
    print(stats)

def display_barplot (dataframe:pd.DataFrame, column_name_x:str, column_name_y:str, hue:str):
    """
    Parameter : 
        - dataframe : The dataframe database
        - column_name_x : colum name on abscissa
        - column_name_y : colum name on ordinate
        
    Procedure : 
        Display a bar graph
    """   

    if column_name_x not in dataframe.columns or column_name_y not in dataframe.columns:
        raise ValueError(f"Column '{column_name_x}' or '{column_name_y}' not found in the DataFrame.")

    part_x = dataframe[column_name_x].value_counts()
    part_y = dataframe[column_name_y].value_counts()

    if part_x.empty or part_y.empty:
        raise ValueError(f"Column '{column_name_x}' or '{column_name_y}' has no data to display in the barplot.")

    sns.barplot(data=dataframe, x=column_name_x, y=column_name_y, hue = hue, palette = "pastel")
    plt.xticks(rotation=45, ha="right")
    plt.ylim(15, None)
    plt.title(f"Barplot of {column_name_x} and {column_name_y} by {hue}")
    plt.show()
    plt.close()

def display_kde (dataframe:pd.DataFrame, column_name, hue):
    """
    Parameter : 
        - dataframe : The dataframe database
        - column_name : colum name on abscissa
        - title : chart title
        
    Procedure : 
        Display a Kernel Density Estimate (KDE) plot for a continuous variable (`column_name`), 
        with different categories (based on `hue`) represented by different colors.
    """   

    if column_name not in dataframe.columns:
        raise ValueError(f"Column '{column_name}' not found in the DataFrame.") 
    part = dataframe[column_name].value_counts()
    if part.empty:
        raise ValueError(f"Column '{column_name}' has no data to display in the barplot.")

    sns.kdeplot(data=dataframe, x=column_name, hue=hue, fill=True)
    plt.title(f"Kernel Density Estimate of {column_name} by {hue}")
    plt.show()
    plt.close()


def display_stacked_bars (dataframe:pd.DataFrame):
    """
    Parameter : 
        The dataframe database
    
    Procedure : 
        Display a stacked bars graphic
    """
    
    if not isinstance(dataframe, pd.DataFrame):
        raise ValueError("The provided input is not a valid DataFrame.")
    if dataframe.empty:
        raise ValueError("The DataFrame is empty. Cannot generate a stacked bar plot.")
    numeric_columns = dataframe.select_dtypes(include=['number']).columns
    if len(numeric_columns) == 0:
        raise ValueError("The DataFrame does not contain any numeric columns to display in a stacked barplot.")

    dataframe.plot(kind='bar', stacked=True, figsize=(10, 6), color=["red", "green"])
    plt.ylabel("Numbers")
    plt.title(f"Distribution by Tuition fees up to date")
    plt.show()
    plt.close()


def display_countplot (dataframe:pd.DataFrame, column_name:str, hue:str):
    """
    Parameter : 
        - dataframe : The dataframe database.
        - column_name : The column name to be plotted on the x-axis.
        - hue : The categorical variable that will be used to differentiate the bars by color.
        
    Procedure : 
        Display a countplot (barplot) for a categorical variable
    """ 
    
    if not isinstance(dataframe, pd.DataFrame):
        raise ValueError("The provided input is not a valid DataFrame.")
    if column_name not in dataframe.columns:
        raise ValueError(f"Column '{column_name}' not found in the DataFrame.")
    if hue not in dataframe.columns:
        raise ValueError(f"Column '{hue}' not found in the DataFrame.")
    if not isinstance(dataframe[column_name].dtype, pd.CategoricalDtype) and not dataframe[column_name].dtype == 'object':
        raise ValueError(f"Column '{column_name}' should be a categorical or object type.")
    if not isinstance(dataframe[hue].dtype, pd.CategoricalDtype) and not dataframe[hue].dtype == 'object':
        raise ValueError(f"Column '{hue}' should be a categorical or object type.")
    
    
    sns.countplot(x=column_name, hue=hue, data=dataframe, palette = "pastel")
    plt.xticks(rotation=45, ha="right")
    plt.title(f"Distribution of {column_name} by {hue}")
    plt.show()
    plt.close()
    
def display_boxplot(dataframe:pd.DataFrame, column_name_1:str, column_name_2:str):
    """
    Parameter : 
        - dataframe : The dataframe database
        - column_name_1 : colum name number 1
        - column_name_2 : colum name number 2 
        
    Procedure : 
        Display a boxplot
    """ 

    if not isinstance(dataframe, pd.DataFrame):
        raise ValueError("The provided input is not a valid DataFrame.")    
    if column_name_1 not in dataframe.columns:
        raise ValueError(f"Column '{column_name_1}' not found in the DataFrame.")
    if column_name_2 not in dataframe.columns:
        raise ValueError(f"Column '{column_name_2}' not found in the DataFrame.")
    if not isinstance(dataframe[column_name_1].dtype, pd.CategoricalDtype) and not dataframe[column_name_1].dtype == 'object':
        raise ValueError(f"Column '{column_name_1}' should be categorical or of type object for a boxplot.")
    if not (dataframe[column_name_2].dtype, pd.CategoricalDtype) and not dataframe[column_name_2].dtype == 'object':
        raise ValueError(f"Column '{column_name_2}' should be numeric for a boxplot.")
    
    sns.boxplot(x = column_name_1, y = column_name_2, data = dataframe, palette = "pastel")
    plt.title(f"Boxplot of {column_name_2} by {column_name_1}")
    plt.show()
    plt.close()
    
    
def display_scatterplot (dataframe:pd.DataFrame, column_name_1:str, column_name_2:str, hue:str):
    """
    Parameter : 
        - dataframe : The dataframe database
        - column_name_1 : colum name number 1
        - column_name_2 : colum name number 2 
        
    Procedure : 
        Display a scatterplot
    """ 
    
    if not isinstance(dataframe, pd.DataFrame):
        raise TypeError("The input dataframe must be a pandas DataFrame.")
    if column_name_1 not in dataframe.columns:
        raise ValueError(f"Column '{column_name_1}' not found in the DataFrame.")
    if column_name_2 not in dataframe.columns:
        raise ValueError(f"Column '{column_name_2}' not found in the DataFrame.")
    if hue not in dataframe.columns:
        raise ValueError(f"Column '{hue}' not found in the DataFrame.")
    if dataframe[column_name_1].isnull().any():
        raise ValueError(f"Column '{column_name_1}' contains missing values.")
    if dataframe[column_name_2].isnull().any():
        raise ValueError(f"Column '{column_name_2}' contains missing values.")
    if not isinstance(dataframe[hue].dtype, pd.CategoricalDtype):
        dataframe[hue] = dataframe[hue].astype('category')
        
    sns.scatterplot(
        data=dataframe,
        x=column_name_1,
        y=column_name_2,
        hue=hue,
        palette="coolwarm",
    )
    plt.xlabel(column_name_1)
    plt.ylabel(column_name_2)
    plt.title("Scatter Plot of {column_name_1} vs {column_name_2} by {hue}")
    plt.show()
    plt.close()

##########################################
######             Main             ######
##########################################

if __name__ == "__main__":
    # Data import
    path = "../../data/"
    df = run_data(path)
        
    # Mapping
    mappings = do_mapping()
    for column, mapping in mappings.items():
       if column in df.columns:  # Checks whether the column exists in the DataFrame
         df[column] = df[column].replace(mapping)

    # Preparing and cleaning the database
    check_na(df)
    check_types(df)
    display_dataset(df)

    # Descriptives statistics
    calculate_desc_stat(df, "Gender", "Curricular units 2nd sem (enrolled)")
    calculate_desc_stat(df, "Gender", "Curricular units 2nd sem (evaluations)")
    calculate_desc_stat(df, "Gender", "Age at enrollment")
    calculate_desc_stat(df, "Course", "Age at enrollment")
    calculate_desc_stat(df, "Tuition fees up to date", "Age at enrollment")
    calculate_desc_stat(df, "Scholarship holder", "Age at enrollment")
    calculate_desc_stat(df, "Marital status", "Age at enrollment")

    # Graphics display
    display_barplot(df, "Course", "Age at enrollment", "Gender")
    display_kde(df, "Age at enrollment", "Gender")
    display_stacked_bars(do_crosstab(df, "Course", "Tuition fees up to date"))
    display_stacked_bars(do_crosstab(df, "Age at enrollment", "Tuition fees up to date"))
    display_countplot(df, "Course", "Target")
    display_countplot(df, "Displaced", "Gender")
    display_boxplot(df, "Displaced", "Age at enrollment")
    display_boxplot(df, "Displaced", "Unemployment rate")
    display_boxplot(df, "Displaced", "Inflation rate")
    display_boxplot(df, "Displaced", "GDP")
    display_boxplot(df, "Scholarship holder", "Age at enrollment")
    display_boxplot(df, "Tuition fees up to date", "Age at enrollment")
    display_scatterplot(df, "Age at enrollment", "Curricular units 2nd sem (enrolled)", "Gender")



