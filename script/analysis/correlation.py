import sys
import os 
sys.path.append(os.path.abspath(os.path.join(os.path.dirname(__file__), '..')))
sys.path.append(os.path.abspath(os.path.join(os.path.dirname(__file__), '..', 'config')))

# Importation des libraries 
import matplotlib.pyplot as plt
import seaborn as sns
import pandas as pd
import numpy as np
from fanalysis.ca import CA  # pour les AFC
from scipy.stats import chi2_contingency
from config.constants import df_mapping, VAR_PRED,QUAL_VARS, QUANT_VARS,MAPPING_Y
from typing import Tuple
from sklearn.preprocessing import StandardScaler
from scipy.stats import f_oneway
from modele_prediction import TargetEstimator

def plot_correlation_heatmap(correlation_matrix: pd.DataFrame) -> None:
    """
    Plots the heatmap of the correlation matrix.
    """
    mask = np.triu(np.ones_like(correlation_matrix, dtype=bool))
    #plt.figure(figsize=(6, 4))  # Ajuste la taille de la figure
    plt.tight_layout()  # Réduit les espaces inutiles
    heatmap = sns.heatmap(correlation_matrix, 
                        mask=mask, 
                        annot=False,
                        cmap="BuGn", 
                        linewidths=0.5, 
                        cbar=True)
    heatmap.set_xticklabels(heatmap.get_xticklabels(), rotation=45, horizontalalignment='right', fontsize=5)
    heatmap.set_yticklabels(heatmap.get_yticklabels(), rotation=0, fontsize=5)
    plt.tight_layout()
    #plt.title("Matrice de corrélation", y=0.9, fontsize=10)


def chi2_test_on_crosstable(table_croisee: pd.DataFrame, var: str) -> None:
    """
    Performs Chi-squared test on the provided contingency table.
    """
    print("------------------------------------------------------------------------------")
    print(f"--------------------------------{var}--------------------------------")
    print("------------------------------------------------------------------------------")

    chi2_stat, p_val, dof, expected = chi2_contingency(table_croisee)
    print(f"P-value du test du khi-deux pour {var}: {p_val:.1e}")
    print(f"Stat du test du khi-deux pour {var}: {chi2_stat:.1e}")
        # Interpretation
    if p_val < 0.05:
        print("Conclusion: There is a significant difference between the groups.")
    else:
        print("Conclusion: No significant difference detected between the groups.")


def create_crosstable(df: pd.DataFrame, var: str) -> pd.DataFrame:
    """
    Creates and returns a contingency table for the given variable.
    """
    table_croisee = pd.crosstab(df[VAR_PRED[0]], df[var])
    return table_croisee


def generate_barplot_for_categorical(df: pd.DataFrame, var: str) -> None:
    """
    Generates a barplot to visualize proportions for categorical variables.
    """
    table_croisee = pd.crosstab(df[VAR_PRED[0]], df[var])
    # Profils lignes
    profil_ligne = table_croisee.div(table_croisee.sum(axis=1), axis=0)
    profil_ligne["Total"] = profil_ligne.sum(axis=1)

    # Profils colonnes
    profil_colonne = table_croisee.div(table_croisee.sum(axis=0), axis=1)
    profil_colonne.loc["Total"] = profil_colonne.sum(axis=0)

    # Extraction des noms de colonnes
    col_names = list(table_croisee.columns)
    total = table_croisee[col_names].sum()
    prop = total / total.sum()
    prop_sort = prop.sort_values(ascending=False)  # Tri décroissant des proportions
    # Création du barplot avec un tri explicite
    fig, ax = plt.subplots(figsize=(20, 6))
    bars = ax.bar(prop_sort.index, prop_sort.values, color='skyblue') 

    # Ajout des annotations
    for bar, value in zip(bars, prop_sort.values):  
        height = bar.get_height()
        ax.annotate(f'{value:.2f}',  # Format des valeurs
                    xy=(bar.get_x() + bar.get_width() / 2, height),
                    xytext=(0, 3),  # Légère marge au-dessus de la barre
                    textcoords="offset points",
                    ha='center', va='bottom', fontsize=8)
    # Mise à jour des axes
    ax.set_xticks(range(len(prop.index)))
    ax.set_xticklabels(prop_sort.index, rotation=90)
    ax.set_xlabel(f'Type de {var}')
    ax.set_ylabel("Proportion d'étudiants")
    ax.set_title(f'Répartition des étudiants par {var} (par ordre décroissant)')

    # Affichage du graphique
    plt.tight_layout()
    plt.show()

def encode(df: pd.DataFrame, intercept : bool) -> Tuple[pd.DataFrame, pd.DataFrame]:
    """
    Encodes the categorical variables in the dataframe and scales the quantitative variables.

    Parameters:
    - df : pd.DataFrame : The input dataframe to encode.

    Returns:
    - X_encoded : pd.DataFrame : Encoded and scaled independent variables.
    - y : pd.DataFrame : The target variable.
    """
    df[VAR_PRED[0]] = df[VAR_PRED[0]].astype('category')
    y = df[VAR_PRED[0]].map(MAPPING_Y)

    X_encoded = pd.get_dummies(df[QUANT_VARS + QUAL_VARS], columns=QUAL_VARS, drop_first=True)
    X_encoded = X_encoded.astype(int)
    
    for column in QUANT_VARS:
        ss = StandardScaler()
        X_encoded[column] = ss.fit_transform(X_encoded[[column]])

    if intercept == True:
        X_encoded = pd.concat([pd.Series(1, index=X_encoded.index, name='const'), X_encoded], axis=1)
    return X_encoded, y



def impact_encode_multiclass(X: pd.DataFrame, categorical_columns: list) -> pd.DataFrame:
    """
    Encodes categorical variables based on their impact on the target variable (multiclass encoding).
    """
    X_encoded = X.drop(columns=[VAR_PRED]).copy()

    for column in categorical_columns:
        encoding = X.groupby(column, observed=False)[VAR_PRED].value_counts(normalize=True).unstack(fill_value=0)
        for class_name in encoding.columns:
            X_encoded[f"{column}_{class_name}"] = X[column].map(encoding[class_name])

    return X_encoded


def perform_afr_analysis(df: pd.DataFrame, table_croisee: pd.DataFrame) -> None:
    """
    Performs the AFC (Correspondence Analysis) and returns results.
    """
    my_ca = CA(row_labels=df[VAR_PRED[0]].unique(), col_labels=table_croisee.columns.values, stats=True)
    my_ca.fit(table_croisee.values)

    # Graphique des valeurs propres
    info_target = my_ca.row_topandas()
    info_target["Masse"] = table_croisee.sum(axis=1) / table_croisee.values.sum()
    info_colonne = my_ca.col_topandas()
    info_colonne["Masse"] = table_croisee.sum(axis=0) / table_croisee.values.sum()
    info_colonne = info_colonne.round(2)
    columns_to_normalize = ['col_contrib_dim2', 'col_contrib_dim1']
    info_colonne[columns_to_normalize] = info_colonne[columns_to_normalize].div(info_colonne[columns_to_normalize].sum(axis=0), axis=1).round(2)

    info_target = info_target.round(2)
    columns_to_normalize = ['row_contrib_dim2', 'row_contrib_dim1']
    info_target[columns_to_normalize] = info_target[columns_to_normalize].div(info_target[columns_to_normalize].sum(axis=0), axis=1).round(2)


    # Exporter vers CSV avec un séparateur spécifique
    #info_colonne.to_csv(f"info_{VAR_PRED[0]}.csv", sep=';', index=True, header=True)


def process_data(df: pd.DataFrame) -> pd.DataFrame:
    """
    Preprocess and prepare the data, including encoding and filtering.
    """
    df[VAR_PRED[0]]= df[VAR_PRED[0]].astype('category')
    df = df[VAR_PRED + QUANT_VARS + QUAL_VARS]
    return df


def perform_correlation_analysis(df_encoded: pd.DataFrame) -> None:
    """
    Perform correlation analysis and plot heatmap for the given quantitative variables.
    """
    correlation_matrix = df_encoded[QUANT_VARS].corr()
    correlation_matrix = df_encoded.corr()
    plot_correlation_heatmap(correlation_matrix)

    high_corr = correlation_matrix.unstack().drop_duplicates()
    high_corr = high_corr[(abs(high_corr) > 0.7)]

    print("Paires de variables avec une corrélation supérieure à 0.5 :")
    print(high_corr)


def anova_test(df, qualitative_var, quantitative_var):
    """
    Performs an ANOVA test between a qualitative variable and a quantitative variable.

    Parameters:
    - df: pandas DataFrame containing the data.
    - qualitative_var: Name of the qualitative variable (string).
    - quantitative_var: Name of the quantitative variable (string).

    Returns:
    - ANOVA test result (F-statistic and p-value).
    - Prints group means and test conclusion.
    """
    # Check the modalities of the qualitative variable
    groups = df.groupby(qualitative_var, observed = False)[quantitative_var].apply(list)
    
    # Calculate means per group
    mean_per_group = df.groupby(qualitative_var, observed = False)[quantitative_var].mean()
    print("------------------------------------------------------------------------------")
    print(f"--------------------------------{quantitative_var}--------------------------------")
    print("------------------------------------------------------------------------------")

    print("Means per group:")
    print(mean_per_group)
    
    # Perform ANOVA test
    f_stat, p_value = f_oneway(*groups)
    
    print("\nANOVA Test Results:")
    print(f"F-statistic: {f_stat:.4f}")
    print(f"p-value: {p_value:.2e}")
    
    # Interpretation
    if p_value < 0.05:
        print("Conclusion: There is a significant difference between the groups.")
    else:
        print("Conclusion: No significant difference detected between the groups.")
    
    return f_stat, p_value

def main() -> None:
    
    target_estimator = TargetEstimator()
    
    # Load data
    df = target_estimator.load_data()

    # Data preparation
    df = process_data(df)
    

    # Analysis of qualitative variables
    for var in QUAL_VARS:
        #generate_barplot_for_categorical(df, var)
        table_croisee = create_crosstable(df,var)
        chi2_test_on_crosstable(table_croisee, var)
        # if df[var].nunique() > 2:
        #     perform_afr_analysis(df, table_croisee)

    # Analysis of qualitative variables

    for var in QUANT_VARS:
        #plt.figure()
        means = df.groupby(VAR_PRED[0], observed = False)[var].mean()  # Calculation of averages for each group
        # for group, mean in means.items():
        #     plt.scatter(group, mean, color='red', s=100, zorder=5, label=f"Moyenne {group}" if group == 0 else "")
        # sns.boxplot(x=VAR_PRED[0], y=var, data=df)
        # plt.title(f'Boxplot of {var} by Target')
        # if group == 0:
        #     plt.legend()
        anova_test(df, VAR_PRED[0], var)

    print(df.columns)
    # Preparing data for prediction
    df_encoded, y = encode(df, intercept = False)

   # Correlation analysis
    perform_correlation_analysis(df_encoded)
  
    plt.show()


if __name__ == "__main__":
    main()
