##########################################
####         Import libraries        #####
##########################################

import os
import pandas as pd
import matplotlib.pyplot as plt

##########################################
######          Functions           ######
##########################################

def run_data (path:str):
    """
    Parameter : 
        Path to access data set

    Function : 
        Run data set
    
    Return : 
        The dataframe database
    """
    
    df = None
    try :
        df = pd.read_csv(os.path.join(path,'dataset.csv'), delimiter = ",")
    except : 
        print ("File not found")
    return df

def select_binaries_var (dataframe:pd.DataFrame):
    """
    Parameter : 
        The dataframe database
    
    Function : 
        Selects binary modalities variables
    
    Return : 
        An Index variable containing the names of columns containing binary modalities
    """
    
    columns_bin = dataframe.columns[dataframe.isin([0,1]).all()]
    return columns_bin

##########################################
######          Procedures          ######
##########################################

def check_na (dataframe:pd.DataFrame):
    """
    Parameter : 
        The dataframe database
    
    Procedure : 
        Check for missing values
    """
    
    print(dataframe[select_binaries_var(dataframe)].isnull().sum())

def check_types(dataframe:pd.DataFrame):
    """
    Parameter : 
        The dataframe database
    
    Procedure : 
        Check data types
    """
    
    print(dataframe[select_binaries_var(dataframe)].dtypes)

def display_dataset(dataframe:pd.DataFrame):
    """
    Parameter : 
        The dataframe database
    
    Procedure : 
        Display the first 5 lines of the dataset 
    """

    print(dataframe[select_binaries_var(dataframe)].head())

def display_stats_desc(dataframe:pd.DataFrame):
    """
    Parameter : 
        The dataframe database
    
    Procedure : 
        Statistical summary of binary variable observations
    """

    pd.set_option('display.max_columns',None)
    print(dataframe[select_binaries_var(dataframe)].describe(include='all'))
    pd.reset_option('display.max_columns')

def display_pie_chart (dataframe:pd.DataFrame, column_name:str, labels_mapping:dict, title:str):
    """
    Parameter : 
        - dataframe : The dataframe database
        - column_name : colum name which you want create a chart
        - labels_mapping : dictionnary associating numeric values with string labels
        - title : chart title
    
    Procedure : 
        Display a circular graph
    """

    part = dataframe[column_name].value_counts()
    labels = [labels_mapping[i] for i in part.index] # replacing indices with a character string
    plt.pie(part, labels = labels, autopct='%1.1f%%', colors =['c', 'y'])
    plt.title(title)
    plt.show()

##########################################
######             Main             ######
##########################################

if __name__ == "__main__":
    # Data import
    path = "../../data/"
    df = run_data(path)

    # Preparing and cleaning the database
    check_na(df)
    check_types(df)
    display_dataset(df)
    columns_bin = list(select_binaries_var(df))
    columns_bin.remove('Gender') 

    # Descriptives statistics
    display_stats_desc(df)

    # Mapping dictionary
    labels_yesno = {0: 'No', 1: 'Yes'}
    labels_gender = {0: 'Woman', 1: 'Man'}

    # Graphics display
    display_pie_chart(df, 'Gender', labels_gender, "Distribution by gender")
    for columns in columns_bin:
        display_pie_chart(df, columns, labels_yesno, f"Distribution by {columns}")
