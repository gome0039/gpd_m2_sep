# Importation des libraries
import pandas as pd
#from sklearn.linear_model import LogisticRegression
from sklearn.ensemble import RandomForestClassifier
import pickle
import os
from sklearn.preprocessing import StandardScaler
from config.constants import VAR_PRED, QUANT_VARS, QUAL_VARS,MAPPING_Y, X_TRAIN, Y_TRAIN, DATA
from typing import Tuple

class TargetEstimator:
    """
    A class for predicting the likelihood of graduation, reenrollment, or dropout using a logistic regression model.
    """

    def __init__(self):
        """
        Constructor for the TargetEstimator class.
        Initializes encodings and the logistic regression model.
        """
        self.categorical_encoding = None
        self.quantitative_encoding = None
        self.model = None

    def load_data(self):
        """
        Load the dataset from a specified path.

        Parameters:
        - chemin (str): Path to the CSV dataset.

        Returns:
        pandas.DataFrame: The loaded dataset.
        """
        script_directory = os.path.dirname(os.path.abspath(__file__))
        project_directory = os.path.dirname(script_directory)
        raw_data_path = os.path.join(project_directory, "data", DATA)
        df = pd.read_csv(raw_data_path)

        return df

    def encode_variables(self, df:pd.DataFrame)-> Tuple[pd.DataFrame, pd.DataFrame]:
        """
        Preprocess the dataset by encoding qualitative variables and the target variable.

        Parameters:
        - df (pandas.DataFrame): The input dataset.

        Returns:
        tuple: A tuple containing the preprocessed feature matrix (X), 
               the encoded target variable (y), and the column encoding.
        """
        self.categorical_encoding = {}
        self.quantitative_encoding = {}
        
        df['Target'] = df['Target'].replace('Enrolled', 'Dropout')
        # Map target values to numeric categories
        df[VAR_PRED[0]] = df[VAR_PRED[0]].astype('category')
        y = df[VAR_PRED[0]].map(MAPPING_Y)
        

        # Encode qualitative ones
        X_encoded = pd.get_dummies(df[QUANT_VARS + QUAL_VARS], columns=QUAL_VARS, drop_first=True)
        
        self.categorical_encoding = X_encoded.columns[len(QUANT_VARS):]
        X_encoded = X_encoded.astype(int)

        for column in QUANT_VARS:
            ss = StandardScaler()
            self.quantitative_encoding[column] = {'scaler': ss,
                                                'mean': X_encoded[column].mean(), 
                                                'std': X_encoded[column].std()}
            X_encoded[column] = ss.fit_transform(X_encoded[[column]])
        #X_encoded = pd.concat([pd.Series(1, index=X_encoded.index, name='const'), X_encoded], axis=1)   
        
        # Save categorical encoders
        script_directory = os.path.dirname(os.path.abspath(__file__))
        path = os.path.join(script_directory, "save_model_encoders")
        os.makedirs(path, exist_ok=True)
        cat_path = os.path.join(path, 'categorical_encoders.pkl')
        with open(cat_path, 'wb') as file:
            pickle.dump(self.categorical_encoding, file)

        quant_path = os.path.join(path, 'quantitative_encoders.pkl')
        with open(quant_path, 'wb') as file:
            pickle.dump(self.quantitative_encoding, file)

        return X_encoded, y

    def get_modele(self, X:pd.DataFrame, y:pd.DataFrame):
        """
        Train a logistic regression model and save it as a pickle file.

        Parameters:
        - X (pandas.DataFrame): Feature matrix.
        - y (pandas.Series): Target variable.

        Returns:
        sklearn.linear_model.LogisticRegression: The trained model.
        """
        # Split the data into training and testing sets
        path = os.path.join(os.path.dirname(os.path.abspath(__file__)),"models","data_train_test")
        X_train = pd.read_csv(os.path.join(path, X_TRAIN))
        y_train = pd.read_csv(os.path.join(path, Y_TRAIN)).values.ravel()
        # Train a logistic regression model
        model = RandomForestClassifier(n_estimators=500, random_state=42)
        model.fit(X_train, y_train)

        # Save the model
        script_directory = os.path.dirname(os.path.abspath(__file__))
        path = os.path.join(script_directory, "save_model_encoders")
        os.makedirs(path, exist_ok=True)
        model_path = os.path.join(path, 'model.pkl')
        with open(model_path, 'wb') as file:
            pickle.dump(model, file)

        self.model = model


    def get_prediction(self, curr_2nd_eval:int, curr_2nd_enrolled:int, curr_2nd_approved:int,curr_2nd_grade:int,age:int, istuition:int, 
                   isgender:int, isscholarship_holder:int)-> float:
        """
        Predict the probabilities of graduation, reenrollment, or dropout for a given set of features.

        Parameters:
        - curr_2nd_eval (int): Number of curricular units evaluated in the second semester.
        - curr_2nd_enrolled (int): Number of curricular units enrolled in the second semester.
        - age (int): Age at enrollment.
        - inflation (float): Inflation rate (percentage).
        - tuition (int): Tuition fees up to date (1 : yes/ 0:no).
        - marital_status (int): Marital status.
        - mothers_occupation (int): Mother's occupation.
        - gender (int): Gender (e.g., 1 :Male/ 0: Female).
        - course (int): Course type.

        Returns:
        numpy.ndarray: A vector of probabilities for the target categories [Dropout, Enrolled, Graduate].
        """
        # Create a DataFrame with the new data
        data_quanti = pd.DataFrame([{
            'Curricular units 2nd sem (grade)':curr_2nd_grade,
            "Curricular units 2nd sem (approved)": curr_2nd_approved,
            'Curricular units 2nd sem (evaluations)': curr_2nd_eval,
            'Curricular units 2nd sem (enrolled)': curr_2nd_enrolled,
            'Age at enrollment': age,

        }])

        data_quali = pd.DataFrame([{
            'Tuition fees up to date': istuition,
           'Gender': isgender,
           "Scholarship holder" : isscholarship_holder
        }])

         # Load the saved model and the encoders
        script_directory = os.path.dirname(os.path.abspath(__file__))
        path = os.path.join(script_directory, "save_model_encoders")
        os.makedirs(path, exist_ok=True)
        model_path = os.path.join(path, 'model.pkl')
        if not os.path.isfile(model_path):
            raise FileNotFoundError(f"Le fichier modèle '{model_path}' est introuvable.")
        with open(model_path, 'rb') as file:
            self.model = pickle.load(file)
        cat_path = os.path.join(path, 'categorical_encoders.pkl')
        if not os.path.isfile(cat_path):
            raise FileNotFoundError(f"Le fichier des encodeurs catégoriels '{cat_path}' est introuvable.")
        with open(cat_path, 'rb') as file:
            self.categorical_encoding = pickle.load(file)
        quant_path = os.path.join(path, 'quantitative_encoders.pkl')
        if not os.path.isfile(quant_path):
            raise FileNotFoundError(f"Le fichier des encodeurs quantitatifs '{quant_path}' est introuvable.")
        with open(quant_path, 'rb') as file:
            self.quantitative_encoding = pickle.load(file)
        
        new_data_df = data_quali.reindex(columns=self.categorical_encoding, fill_value=0)
        # Parcourir les paramètres
        for var_name in data_quali.columns:
            value = data_quali[var_name].iloc[0] 
            # Construire le nom de la colonne correspondant à la variable
            column_name = f"{var_name}_{value}"    
            # Vérifier si la colonne existe dans le DataFrame
            if column_name in new_data_df.columns:
                # Assigner la valeur 1 à la colonne si elle existe
                new_data_df.loc[0,column_name] = 1
         
        # Encode quantitative variables
        for column in data_quanti.columns:
            mean = self.quantitative_encoding[column]["mean"]
            std = self.quantitative_encoding[column]["std"]
            transformed_value = (data_quanti.iloc[0][column] - mean) / std
            data_quanti[column] = data_quanti[column].astype(float)
            data_quanti.at[0, column] = transformed_value
        new_data_df = pd.concat([data_quanti,new_data_df], axis = 1)
        #new_data_df = pd.concat([pd.Series(1, index=new_data_df.index, name='const'), new_data_df], axis=1)   
      
        # Predict probabilities
        y_pred = self.model.predict_proba(new_data_df)[0]
        return y_pred

